#include <iostream>
#include <fstream>
#include <dophax/projectdata.hpp>
#include <dophax/projectparser.hpp>
#include "templateinstantiation.hpp"

bool parseDependenciesReportFlag(std::string_view arg)
{
    return arg == "--dependencies-report";
}

bool parseDependentsReportFlag(std::string_view arg)
{
    return arg == "--dependents-report";
}

std::optional<std::string> parseWrapDir(std::string_view arg)
{
    return (parseDependenciesReportFlag(arg) || parseDependentsReportFlag(arg)) ? std::optional<std::string>{} : std::optional<std::string>{arg};
}

int main(int argc, char* argv[])
{
    if(argc < 2 || argc > 4)
    {
        std::cout << "must provide project file, like: dophax_gen ../path/to/project.dpx  [optional wrap_dir] [optional --dependencies-report|--dependents-report]\n";
    }
    else
    {
        std::string projectFilePath(argv[1]);
        ProjectData projectData = loadProjectData(projectFilePath);

        //std::cout << "Dophax gen: reading " << projectFilePath << "\n";

        std::string projectRoot = projectFilePath;

        if(std::count(projectRoot.begin(), projectRoot.end(), '/') == 0)
        {
            projectRoot = "./";
        }
        else
        {
            projectRoot.erase(projectRoot.find_last_of('/'));
            projectRoot += "/";
        }

        std::string wrapDir = "";
        RunMode runMode = RunMode::Normal;

        if(argc >= 3)
        {
            bool dependenciesReport = parseDependenciesReportFlag(argv[2]);
            if(dependenciesReport)
                runMode = RunMode::DependenciesReport;
            bool dependentsReport = parseDependentsReportFlag(argv[2]);
            if(dependentsReport)
                runMode = RunMode::DependentsReport;
            auto wrapDirArg = parseWrapDir(argv[2]);

            if(wrapDirArg)
                wrapDir = wrapDirArg->substr(wrapDirArg->find_first_of('=') + 1);
        }
        if(argc == 4)
        {
            bool dependenciesReport = parseDependenciesReportFlag(argv[3]);
            if(dependenciesReport)
                runMode = RunMode::DependenciesReport;
            bool dependentsReport = parseDependentsReportFlag(argv[3]);
            if(dependentsReport)
                runMode = RunMode::DependentsReport;
            auto wrapDirArg = parseWrapDir(argv[3]);

            if(wrapDirArg)
                wrapDir = wrapDirArg->substr(wrapDirArg->find_first_of('=') + 1);
        }

        std::string outputDirectory = wrapDir + "/" + projectData.settings.outputDirectory;

        //std::cout << "Dophax gen: output dir is " << outputDirectory << "\n";

        //if(runMode == RunMode::Normal)
        //    std::cout << "Dophax gen: normal run - will generate files\n";
        //else if(runMode == RunMode::DependenciesReport)
        //    std::cout << "Dophax gen: dependencies report - will print dependency files only\n";
        //else if(runMode == RunMode::DependentsReport)
        //    std::cout << "Dophax gen: dependents report - will print dependent files only\n";

        std::string dependenciesString;

        if(runMode == RunMode::DependenciesReport)
            dependenciesString = projectFilePath + " ";

        for(const std::string& projectTemplate : projectData.settings.templates)
        {
            std::string templateFilePath = projectRoot + "/" + projectTemplate;
            std::ifstream inFile(templateFilePath);
            std::string templateString((std::istreambuf_iterator<char>(inFile)), std::istreambuf_iterator<char>());


            //std::cout << "Dophax gen: reading template file: " << templateFilePath << "\n";
            if(runMode == RunMode::DependentsReport || runMode == RunMode::Normal)
                writeTemplateInstantiations(projectData, outputDirectory, templateString, runMode);
            else if(runMode == RunMode::DependenciesReport)
                dependenciesString += templateFilePath + " ";
        }

        if(!dependenciesString.empty())
            std::cout << dependenciesString;
    }
}
