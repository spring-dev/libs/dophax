#include "templateinstantiation.hpp"
#include <fstream>
#include <filesystem>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <set>
#include <deque>
#include <unordered_set>
#include <dophax/exceptions.hpp>

void replaceAll(std::string& text, const std::unordered_map<std::string, std::string>& replaceMap)
{
    for(const auto& replaceMapping : replaceMap)
    {
        const std::string replaceThis = replaceMapping.first;
        const std::string withThis = replaceMapping.second;
        std::string::size_type iter = 0;
        while((iter = text.find(replaceThis, iter)) != std::string::npos)
        {
            text.replace(iter, replaceThis.size(), withThis);
            iter += withThis.size();
        }
    }
}

std::string stripped(std::string text)
{
    size_t index = 0;

    while((index = text.find_first_of('_')) != std::string::npos)
    {
        text.erase(index, 1);
    }

    return text;
}

std::string camelCase(std::string text)
{
    size_t index = 0;

    if(text.size() > 0)
        text[0] = std::toupper(text[0]);

    while((index = text.find_first_of('_')) != std::string::npos)
    {
        text.erase(index, 1);
        if(index < text.size())
            text[index] = std::toupper(text[index]);
    }

    return text;
}

std::string lowerCamelCase(std::string text)
{
    size_t index = 0;

    while((index = text.find_first_of('_')) != std::string::npos)
    {
        text.erase(index, 1);
        if(index < text.size())
            text[index] = std::toupper(text[index]);
    }

    return text;
}

std::string caps(std::string text)
{
    for(size_t index = 0; index < text.size(); ++index)
        text[index] = std::toupper(text[index]);
    return text;
}

std::string sentence(std::string text)
{
    size_t index = 0;

    if(!text.empty())
    {
        text[0] = std::toupper(text[0]);

        while((index = text.find_first_of('_')) != std::string::npos)
        {
            text[index] = ' ';
        }
    }

    return text;
}

struct Token
{
    enum Type { File, ForEach, End, Text, If, IfNot};
    Type type;
    std::string data;
};

std::string toString(Token::Type type)
{
    if(type == Token::File)
        return "file";
    if(type == Token::ForEach)
        return "foreach";
    if(type == Token::End)
        return "end";
    if(type == Token::Text)
        return "text";
    if(type == Token::If)
        return "if";
    else
        return "ifnot";
}

struct ParsedToken
{
    Token token;
    size_t afterToken;
};

int32_t lineNumber(size_t character, const std::string& text)
{
    int32_t lineCounter = 1;
    for(size_t iter = 0; iter < character; ++iter)
    {
        if(text[iter] == '\n')
            ++lineCounter;
    }

    return lineCounter;
}

Token::Type parseTokenType(size_t tokenStart, const std::string& tokenString)
{
    if(tokenString.find("@file-", tokenStart) == tokenStart)
        return Token::File;
    else if(tokenString.find("@foreach-", tokenStart) == tokenStart)
        return Token::ForEach;
    else if(tokenString.find("@end@", tokenStart) == tokenStart)
        return Token::End;
    else if(tokenString.find("@if-", tokenStart) == tokenStart)
        return Token::If;
    else if(tokenString.find("@ifnot-", tokenStart) == tokenStart)
        return Token::IfNot;
    else
        return Token::Text;
}

size_t findTagClose(size_t tagStart, const std::string& tag, const std::string& tokenString)
{
    size_t candidate = tokenString.find("@", tagStart + 1);

    if(candidate == std::string::npos)
        throw UnclosedTagException(lineNumber(tagStart, tokenString), tag);

    for(size_t iter = tagStart; iter < candidate; ++iter)
    {
        if(std::isspace(tokenString[iter]))
            throw UnclosedTagException(lineNumber(tagStart, tokenString), tag);
    }

    return candidate;
}

size_t findTagStart(size_t position, const std::string& tokenString)
{
    size_t nextFile = tokenString.find("@file-", position);
    size_t nextForEach = tokenString.find("@foreach-", position);
    size_t nextEnd = tokenString.find("@end@", position);
    size_t nextIf = tokenString.find("@if-", position);
    size_t nextIfNot = tokenString.find("@ifnot-", position);

    return std::min<size_t>({nextFile, nextForEach, nextEnd, nextIf, nextIfNot});
}

std::string parseTokenData(size_t tokenStart, Token::Type type, const std::string& tokenString)
{
    if(type == Token::File)
    {
        size_t tagClose = findTagClose(tokenStart, toString(type), tokenString);
        size_t dataStart = tokenStart + 6;
        return tokenString.substr(dataStart, tagClose - dataStart);
    }
    else if(type == Token::ForEach)
    {
        size_t tagClose = findTagClose(tokenStart, toString(type), tokenString);
        size_t dataStart = tokenStart + 9;
        return tokenString.substr(dataStart, tagClose - dataStart);
    }
    else if(type == Token::End)
    {
        findTagClose(tokenStart, toString(type), tokenString);
        return "";
    }
    else if(type == Token::Text)
    {
        size_t nextStart = findTagStart(tokenStart, tokenString);

        if(nextStart == std::string::npos)
            return tokenString.substr(tokenStart);
        else
            return tokenString.substr(tokenStart, nextStart - tokenStart);
    }
    else if(type == Token::If)
    {
        size_t tagClose = findTagClose(tokenStart, toString(type), tokenString);
        size_t dataStart = tokenStart + 4;
        return tokenString.substr(dataStart, tagClose - dataStart);
    }
    else if(type == Token::IfNot)
    {
        size_t tagClose = findTagClose(tokenStart, toString(type), tokenString);
        size_t dataStart = tokenStart + 7;
        return tokenString.substr(dataStart, tagClose - dataStart);
    }
    else
    {
      return "";
    }
}

size_t tokenSize(const Token& token)
{
    if(token.type == Token::File)
    {
        return 6 + token.data.size() + 1;
    }
    else if(token.type == Token::ForEach)
    {
        return 9 + token.data.size() + 1;
    }
    else if(token.type == Token::End)
    {
        return 5;
    }
    else if(token.type == Token::Text)
    {
        return token.data.size();
    }
    else if(token.type == Token::If)
    {
        return 4 + token.data.size() + 1;
    }
    else if(token.type == Token::IfNot)
    {
        return 7 + token.data.size() + 1;
    }
    else
    {
        return -1;
    }
}

ParsedToken parseToken(size_t tokenStart, const std::string& tokenString)
{
    Token::Type newTokenType = parseTokenType(tokenStart, tokenString);
    std::string newTokenData = parseTokenData(tokenStart, newTokenType, tokenString);
    Token token
    {
        newTokenType,
        std::move(newTokenData),
    };
    size_t newTokenSize = tokenSize(token);
    size_t afterToken = tokenStart + newTokenSize;

    if(tokenString[afterToken] == '\n')
        ++afterToken;

    return {std::move(token), afterToken};
}

std::vector<Token> tokenize(std::string tokenString)
{
    std::vector<Token> tokens;

    size_t nextToken = 0;

    while(nextToken < tokenString.size())
    {
        ParsedToken parsedToken = parseToken(nextToken, tokenString);
        tokens.push_back(std::move(parsedToken.token));
        nextToken = parsedToken.afterToken;
    }

    return tokens;
}

std::unordered_set<std::string> gatherTableDependencies(dpx::TableId tableId, const ProjectData& projectData)
{
    std::unordered_set<std::string> dependencyTexts;

    std::set<dpx::TableId> dataTypes;
    std::set<dpx::TableId> fieldTypes;

    forEach([&](dpx::TableId id, const TableColumn& tableColumn)
    {
        if(tableColumn.table == tableId)
        {
            dataTypes.insert(tableColumn.dataType);
            fieldTypes.insert(tableColumn.fieldType);
        }
    }, projectData.tTableColumn);

    for(dpx::TableId dataType : dataTypes)
    {
        const auto& dependencies = get(dataType, projectData.tDataType).dependencies;
        for(const auto& dependencyText : dependencies)
            dependencyTexts.emplace(dependencyText);
    }

    for(dpx::TableId fieldType : fieldTypes)
    {
        const auto& dependencies = get(fieldType, projectData.tFieldType).dependencies;
        for(const auto& dependencyText : dependencies)
            dependencyTexts.emplace(dependencyText);
    }

    const Table& table = get(tableId, projectData.tTable);
    for(const auto& dependency : table.dependencies)
    {
        dependencyTexts.emplace(dependency);
    }
    
    return dependencyTexts;
}

void checkSanity(const std::vector<Token>& tokens)
{
    int32_t frameCounter = 0;

    for(const auto& token : tokens)
    {
        if(token.type == Token::File || token.type == Token::ForEach || token.type == Token::If || token.type == Token::IfNot)
            ++frameCounter;
        else if(token.type == Token::End)
            --frameCounter;
    }

    if(frameCounter != 0)
        throw UnexpectedEndException();
}

struct TokenState
{
    size_t tokenIndex;
    Token::Type type;
    std::string forEachString;
    int32_t forEachIndex;
    int32_t forEachSize;
    std::optional<dpx::TableId> tableId;
};

std::unordered_map<std::string, std::string> replacementMapForEach(const std::string& foreachString, int32_t loopIndex, const ProjectData& projectData, const std::unordered_set<std::string>& dependencies, std::optional<dpx::TableId> tableId)
{
    std::unordered_map<std::string, std::string> result;

    if(foreachString == "dependency")
    {
        auto iter = dependencies.begin();
        std::advance(iter, loopIndex);
        const std::string& dependency = *iter;
        result =
        {
            {"$dependency$", dependency},
        };
    }
    else if(foreachString == "table")
    {
        const Table& table = projectData.tTable.data[loopIndex];
        result =
        {
            {"$table-_name$", table.name},
            {"$table-name$", stripped(table.name)},
            {"$table-Name$", camelCase(table.name)},
            {"$table-lName$", lowerCamelCase(table.name)},
            {"$table-NAME$", caps(table.name)},
            {"$Table name$", sentence(table.name)},
            {"$description$", table.description},
            {"$external-id$", table.externalId ? "true" : "false"},
        };

        if(table.inherits)
        {
            const std::string& parentName = get(*table.inherits, projectData.tTable).name;
            result.emplace("$parent-_name$", parentName);
            result.emplace("$parent-name$", stripped(parentName));
            result.emplace("$parent-Name$", camelCase(parentName));
            result.emplace("$parent-lName$", lowerCamelCase(parentName));
            result.emplace("$parent-NAME$", caps(parentName));
        }
    }
    else if(foreachString == "column")
    {
        std::vector<dpx::TableId> columns;
        forEach([&](dpx::TableId id, const TableColumn& column)
        {
            if(column.table == *tableId)
                columns.push_back(id);
        }, projectData.tTableColumn);

        const TableColumn& tableColumn = get(columns[loopIndex], projectData.tTableColumn);
        std::string tableColumnOutput = generateColumnOutput(columns[loopIndex], projectData);
        result =
        {
            {"$column-_name$", tableColumn.name},
            {"$column-name$", stripped(tableColumn.name)},
            {"$column-Name$", camelCase(tableColumn.name)},
            {"$column-lName$", lowerCamelCase(tableColumn.name)},
            {"$column-NAME$", caps(tableColumn.name)},
            {"$column-output$", tableColumnOutput},
        };
    }
    else if(foreachString == "child")
    {
        std::vector<dpx::TableId> children;
        forEach([&](dpx::TableId id, const Table& table)
        {
            if(table.inherits && *table.inherits == *tableId)
                children.push_back(id);
        }, projectData.tTable);

        const std::string& childName = get(children[loopIndex], projectData.tTable).name;

        result.emplace("$child-_name$", childName);
        result.emplace("$child-name$", stripped(childName));
        result.emplace("$child-Name$", camelCase(childName));
        result.emplace("$child-lName$", lowerCamelCase(childName));
        result.emplace("$child-NAME$", caps(childName));
    }

    return result;
}

int32_t countColumns(dpx::TableId tableId, const ProjectData& projectData)
{
    int32_t counter = 0;

    forEach([&] (dpx::TableId id, const TableColumn& column)
    {
        if(column.table == tableId)
            ++counter;
    }, projectData.tTableColumn);

    return counter;
}

int32_t countChildren(dpx::TableId tableId, const ProjectData& projectData)
{
    int32_t counter = 0;

    forEach([&] (dpx::TableId id, const Table& table)
    {
        if(table.inherits && *table.inherits == tableId)
            ++counter;
    }, projectData.tTable);

    return counter;
}

struct OutFileStream
{
    std::string path;
    std::stringstream data;
};

void writeTemplateInstantiations(const ProjectData& projectData, const std::string& outputDirectory, const std::string& templateString, RunMode mode)
{
    std::string reportString;

    auto tokens = tokenize(templateString);

    checkSanity(tokens);

    std::deque<TokenState> stateStack;
    std::deque<std::unordered_map<std::string, std::string>> replacementStack;

    size_t currentTokenIndex = 0;

    std::optional<OutFileStream> outFile;

    std::unordered_set<std::string> dependencyTexts;

    while(currentTokenIndex < tokens.size())
    {
        const Token& token = tokens[currentTokenIndex];

        if(token.type == Token::Text)
        {
            std::string textToWrite = token.data;

            for(const auto& replacementMap : replacementStack)
                replaceAll(textToWrite, replacementMap);

            if(outFile)
                outFile->data << textToWrite;
            ++currentTokenIndex;
        }
        else if(token.type == Token::File)
        {
            stateStack.push_front(TokenState{currentTokenIndex, token.type, {}, {}, {}, {}});

            std::string outFilePath = outputDirectory + "/" + token.data;

            for(const auto& replacementMap : replacementStack)
                replaceAll(outFilePath, replacementMap);

            outFile =
            OutFileStream{
                outFilePath,
                std::stringstream(),
            };
            ++currentTokenIndex;
        }
        else if(token.type == Token::ForEach)
        {
            int32_t forEachSize = 0;

            std::optional<dpx::TableId> tableId;

            if(token.data == "dependency")
            {
                auto tableIter = std::find_if(stateStack.begin(), stateStack.end(), [] (const TokenState& state) { return state.type == Token::ForEach;});

                if(tableIter == stateStack.end())
                    throw TemplateException("dependency tag found outside of table tag");

                dependencyTexts = gatherTableDependencies(*tableIter->tableId, projectData);

                forEachSize = dependencyTexts.size();
            }
            else if(token.data == "table")
            {
                forEachSize = count(projectData.tTable);
                tableId = projectData.tTable.ids[0];
            }
            else if(token.data == "column")
            {
                auto tableIter = std::find_if(stateStack.begin(), stateStack.end(), [] (const TokenState& state) { return state.type == Token::ForEach;});

                if(tableIter == stateStack.end())
                    throw TemplateException("column tag found outside of table tag");

                tableId = *tableIter->tableId;
                forEachSize = countColumns(*tableId, projectData);
            }
            else if(token.data == "child")
            {
                auto tableIter = std::find_if(stateStack.begin(), stateStack.end(), [] (const TokenState& state) { return state.type == Token::ForEach;});

                if(tableIter == stateStack.end())
                    throw TemplateException("child tag found outside of table tag");

                tableId = *tableIter->tableId;
                forEachSize = countChildren(*tableId, projectData);
            }
            else
                throw TemplateException("unknown foreach tag: " + token.data);

            if(forEachSize > 0)
            {
                stateStack.push_front(TokenState{currentTokenIndex, token.type, token.data, 0, forEachSize, tableId});
                const auto& state = stateStack.front();

                std::unordered_map<std::string, std::string> replacements = replacementMapForEach(state.forEachString, state.forEachIndex, projectData, dependencyTexts, state.tableId);
                replacementStack.push_front(replacements);
            }
            else
            {
                int32_t nestDepth = 1;

                while(nestDepth > 0)
                {
                    ++currentTokenIndex;
                    Token::Type type = tokens[currentTokenIndex].type;

                    if(type == Token::ForEach || type == Token::If || type == Token::IfNot || type == Token::File)
                        ++nestDepth;
                    else if(type == Token::End)
                        --nestDepth;
                }
            }
            ++currentTokenIndex;
        }
        else if(token.type == Token::If || token.type == Token::IfNot)
        {
            const std::string& expression = token.data;
            
            bool ifNot = token.type == Token::IfNot;

            bool isTrue = false;
            if(expression == "has_columns")
            {
                auto tableIter = std::find_if(stateStack.begin(), stateStack.end(), [] (const TokenState& state) { return state.type == Token::ForEach;});

                if(tableIter == stateStack.end())
                    throw TemplateException("has_columns expression found outside of foreach table");

                dpx::TableId tableId = *tableIter->tableId;

                int32_t columnAmount = countIf([&] (dpx::TableId, const TableColumn& tableColumn)
                {
                    return tableColumn.table == tableId;
                }, projectData.tTableColumn);

                isTrue = columnAmount > 0;
            }
            else if(expression == "is_parent")
            {
                auto tableIter = std::find_if(stateStack.begin(), stateStack.end(), [] (const TokenState& state) { return state.type == Token::ForEach;});

                if(tableIter == stateStack.end())
                    throw TemplateException("is_parent expression found outside of foreach table");

                dpx::TableId tableId = *tableIter->tableId;
                isTrue = !children(tableId, projectData).empty();
            }
            else if(expression == "is_child")
            {
                auto tableIter = std::find_if(stateStack.begin(), stateStack.end(), [] (const TokenState& state) { return state.type == Token::ForEach;});

                if(tableIter == stateStack.end())
                    throw TemplateException("is_parent expression found outside of foreach table");

                dpx::TableId tableId = *tableIter->tableId;
                isTrue = get(tableId, projectData.tTable).inherits.has_value();
            }
            else if(expression == "has_external_id")
            {
                auto tableIter = std::find_if(stateStack.begin(), stateStack.end(), [] (const TokenState& state) { return state.type == Token::ForEach;});

                if(tableIter == stateStack.end())
                    throw TemplateException("has_external_id expression found outside of foreach table");

                dpx::TableId tableId = *tableIter->tableId;
                isTrue = get(tableId, projectData.tTable).externalId;
            }
            else if(expression.find("has_tag=") == 0)
            {
                std::string tag(expression.begin() + 8, expression.end());

                auto tableIter = std::find_if(stateStack.begin(), stateStack.end(), [] (const TokenState& state) { return state.type == Token::ForEach;});

                if(tableIter == stateStack.end())
                    throw TemplateException("has_tag= expression found outside of foreach table");

                dpx::TableId tableId = *tableIter->tableId;
                const std::vector<std::string>& tags = get(tableId, projectData.tTable).tags;
                
                if(std::find(tags.begin(), tags.end(), tag) != tags.end())
                {
                    isTrue = true;
                }
            }

            if(ifNot)
                isTrue = !isTrue;

            if(isTrue)
            {
                stateStack.push_front(TokenState{currentTokenIndex, token.type, {}, {}, {}, {}});
            }
            else
            {
                int32_t nestDepth = 1;

                while(nestDepth > 0)
                {
                    ++currentTokenIndex;
                    Token::Type type = tokens[currentTokenIndex].type;

                    if(type == Token::ForEach || type == Token::If || type == Token::IfNot || type == Token::File)
                        ++nestDepth;
                    else if(type == Token::End)
                        --nestDepth;
                }
            }
            ++currentTokenIndex;
        }
        else if(token.type == Token::End)
        {
            Token::Type currentType = stateStack.front().type;
            size_t nextTokenIndex = currentTokenIndex + 1;

            if(currentType == Token::File)
            {
                if(mode == RunMode::Normal)
                {
                    bool equalExists = false;

                    {
                        std::ifstream existingFile(outFile->path);
                        if(existingFile)
                        {
                            std::string oldData((std::istreambuf_iterator<char>(existingFile)), std::istreambuf_iterator<char>());
                            if(oldData == outFile->data.str())
                                equalExists = true;
                        }
                    }

                    if(!equalExists)
                    {
                        std::filesystem::path path = outFile->path;
                        std::filesystem::create_directories(path.parent_path());

                        std::ofstream finalOutFile = std::ofstream(outFile->path);
                        if(!finalOutFile)
                            throw InvalidFileException(InvalidFileException::Write, outFile->path);

                        finalOutFile << outFile->data.str();
                    }
                }
                else if(mode == RunMode::DependentsReport)
                {
                    reportString += outFile->path + " ";
                }

                outFile = {};
                stateStack.pop_front();
            }
            else if(currentType == Token::ForEach)
            {
                replacementStack.pop_front();
                TokenState& state = stateStack.front();
                ++state.forEachIndex;

                bool loopEnded = state.forEachIndex >= state.forEachSize;

                if(loopEnded)
                    stateStack.pop_front();
                else
                {
                    nextTokenIndex = state.tokenIndex + 1;

                    std::unordered_map<std::string, std::string> replacements = replacementMapForEach(state.forEachString, state.forEachIndex, projectData, dependencyTexts, state.tableId);

                    replacementStack.push_front(replacements);
                    if(state.forEachString == "table")
                        state.tableId = projectData.tTable.ids[state.forEachIndex];
                }
            }
            else if(currentType == Token::If || currentType == Token::IfNot)
            {
                stateStack.pop_front();
            }

            currentTokenIndex = nextTokenIndex;
        }
    }

    if(!reportString.empty())
    {
        std::cout << reportString;
    }
}
