#pragma once
#include <string>
#include <dophax/projectdata.hpp>

enum class RunMode
{
    Normal, //generate the files as normal
    DependenciesReport, //print to stdout a list of files that this run depends on. typically the .dpx and the .dpt files. no files are generated
    DependentsReport, //print to stdout a list of files that are dependent on this run. typically the generated files. no files are generated
};

void writeTemplateInstantiations(const ProjectData& projectData, const std::string& outputDirectory, const std::string& templateString, RunMode mode);
