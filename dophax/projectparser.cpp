#include "projectparser.hpp"
#include "exceptions.hpp"
#include <nlohmann/json.hpp>
#include <fstream>

ProjectData loadProjectData(const std::string& filePath)
{
    std::ifstream file(filePath);

    if(file)
    {
            ProjectData result;
            result.projectFile = filePath;

            std::unordered_map<std::string, std::string> inheritances;

            nlohmann::json projectJson;
            file >> projectJson;

            //settings
            nlohmann::json settingsJson = projectJson["settings"];

            result.settings.outputDirectory = settingsJson["output_directory"];
            result.settings.humanReadable = settingsJson["human_readable"];
            nlohmann::json templatesJson = settingsJson["templates"];

            for(auto projectTemplate : templatesJson)
            {
                result.settings.templates.push_back(projectTemplate);
            }

            //data types
            nlohmann::json dataTypesJson = projectJson["data_types"];
            for(auto dataType : dataTypesJson)
            {
                std::vector<std::string> dependencies = dataType["dependencies"];

                insert(DataType
                {
                    dataType["name"],
                    dataType["output"],
                    std::move(dependencies),
                }, result.tDataType);
            }

            //field types
            nlohmann::json fieldTypesJson = projectJson["field_types"];
            for(auto fieldType : fieldTypesJson)
            {
                std::vector<std::string> dependencies = fieldType["dependencies"];

                insert(FieldType
                {
                    fieldType["name"],
                    fieldType["output"],
                    std::move(dependencies),
                }, result.tFieldType);
            }

            //tables
            nlohmann::json tablesJson = projectJson["tables"];
            for(auto tableJson : tablesJson)
            {
                std::vector<std::string> dependencies;
                if(tableJson.count("dependencies") != 0)
                {
                    std::vector<std::string> temp = tableJson["dependencies"];
                    dependencies = std::move(temp);
                }

                std::vector<std::string> tags;
                if(tableJson.count("tags") != 0)
                {
                    std::vector<std::string> temp = tableJson["tags"];
                    tags = std::move(temp);
                }

                std::string tableName = tableJson["name"];
                dpx::TableId createdTable = insert(Table
                {
                    tableName,
                    tableJson["description"],
                    std::move(dependencies),
                    std::move(tags),
                    tableJson["external_id"],
                    {},
                }, result.tTable).id;

                for(auto columnJson : tableJson["columns"])
                {
                    dpx::TableId createdColumn = insert(TableColumn
                    {
                        createdTable,
                        columnJson["name"],
                        idFromName(columnJson["data_type"], result.tDataType),
                        idFromName(columnJson["field_type"], result.tFieldType),
                    }, result.tTableColumn).id;

                    for(auto fieldTypeVariableJson : columnJson["field_type_variables"])
                        insert(FieldTypeVariable
                        {
                            createdColumn,
                            fieldTypeVariableJson["name"],
                            fieldTypeVariableJson["value"],
                        }, result.tFieldTypeVariable);
                }

                nlohmann::json inheritsJson = tableJson["inherits"];
                if(!inheritsJson.is_null())
                {
                    std::string inherits = inheritsJson;
                    inheritances.emplace(tableName, inherits);
                }
            }

            //resolve inheritance now when all table names are known

            forEach([&] (Table& table)
            {
                auto inheritsIter = inheritances.find(table.name);
                if(inheritsIter != inheritances.end())
                {
                    table.inherits = idFromName(inheritsIter->second, result.tTable);
                }
            }, result.tTable);

            return result;
    }
    else
    {
        throw InvalidFileException(InvalidFileException::Read, filePath);
    }
}

void saveProjectData(const std::string& filePath, const ProjectData& projectData)
{
    nlohmann::json projectJson;

    //settings
    projectJson["settings"] =
    {
        {"output_directory", projectData.settings.outputDirectory},
        {"human_readable", projectData.settings.humanReadable},
        {"templates", projectData.settings.templates},
    };

    //data types
    nlohmann::json dataTypes = nlohmann::json::array();
    forEach([&](dpx::TableId id, const DataType& type)
    {
        dataTypes.push_back(
        {
            {"name", type.name},
            {"output", type.output},
            {"dependencies", type.dependencies},
        });   
    }, projectData.tDataType);
    projectJson["data_types"] = dataTypes;

    //field types
    nlohmann::json fieldTypes = nlohmann::json::array();
    forEach([&](dpx::TableId id, const FieldType& type)
    {
        fieldTypes.push_back(
        {
            {"name", type.name},
            {"output", type.output},
            {"dependencies", type.dependencies},
        });   
    }, projectData.tFieldType);
    projectJson["field_types"] = fieldTypes;
    
    //tables
    nlohmann::json tables = nlohmann::json::array();
    forEach([&](dpx::TableId id, const Table& table)
    {
        nlohmann::json columns = nlohmann::json::array();

        forEach([&](dpx::TableId columnId, const TableColumn& column)
        {
            nlohmann::json fieldTypeVariables = nlohmann::json::array();

            forEach([&] (dpx::TableId variableId, const FieldTypeVariable& variable)
            {
                if(variable.column == columnId)
                {
                    fieldTypeVariables.push_back(
                    {
                        {"name", variable.name},
                        {"value", variable.value},
                    });
                }
            }, projectData.tFieldTypeVariable);
            if(column.table == id)
            {
                columns.push_back(
                {
                    {"name", column.name},
                    {"data_type", get(column.dataType, projectData.tDataType).name},
                    {"field_type", get(column.fieldType, projectData.tFieldType).name},
                    {"field_type_variables", fieldTypeVariables},
                });
            }
        }, projectData.tTableColumn);

        nlohmann::json inherits;

        if(table.inherits)
            inherits = get(*table.inherits, projectData.tTable).name;

        tables.push_back(
        {
            {"name", table.name},
            {"description", table.description},
            {"dependencies", table.dependencies},
            {"tags", table.tags},
            {"external_id", table.externalId},
            {"columns", columns},
            {"inherits", inherits},
        });   

    }, projectData.tTable);
    projectJson["tables"] = tables;

    std::ofstream outFile(filePath);

    if(outFile)
    {
        outFile << projectJson.dump(4);
    }
    else
    {
        throw InvalidFileException(InvalidFileException::Write, filePath);
    }
}
