#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include <optional>
#include <list>
#include <functional>
#include <dpx/tables.hpp>
#include <dpx/insert.hpp>
#include <dpx/find.hpp>
#include <dpx/get.hpp>
#include <dpx/has.hpp>
#include <dpx/count.hpp>
#include <dpx/erase.hpp>
#include <dpx/foreach.hpp>
#include "exceptions.hpp"

struct Settings
{
    std::string outputDirectory;
    bool humanReadable;
    std::vector<std::string> templates;
};

struct DataType
{
    std::string name;
    std::string output;
    std::vector<std::string> dependencies;
};
using DataTypeTable = dpx::DataTable<DataType, false>;

struct FieldType
{
    std::string name;
    std::string output;
    std::vector<std::string> dependencies;
};
using FieldTypeTable = dpx::DataTable<FieldType, false>;

struct Table
{
    std::string name;
    std::string description;
    std::vector<std::string> dependencies;
    std::vector<std::string> tags;
    bool externalId;
    std::optional<dpx::TableId> inherits;
};
using TableTable = dpx::DataTable<Table, false>;

struct TableColumn
{
    dpx::TableId table;
    std::string name;
    dpx::TableId dataType;
    dpx::TableId fieldType;
};
using TableColumnTable = dpx::DataTable<TableColumn, false>;

struct FieldTypeVariable
{
    dpx::TableId column;
    std::string name;
    std::string value;
};
using FieldTypeVariableTable = dpx::DataTable<FieldTypeVariable, false>;

struct ProjectData
{
    //meta
    std::string projectFile;

    //loaded
    Settings settings;
    std::vector<std::string> extensions;

    FieldTypeVariableTable tFieldTypeVariable;
    TableColumnTable tTableColumn;
    TableTable tTable;
    FieldTypeTable tFieldType;
    DataTypeTable tDataType;
};

template <typename Table>
std::vector<dpx::TableId> getTableColumns(dpx::TableId tableId, const Table& columns)
{
    std::vector<dpx::TableId> ids;
    findOne([&](dpx::TableId id, const typename Table::Type& tableEntry)
    {
        if(tableEntry.table == tableId)
                ids.push_back(id);

        return false;
    }, columns);

    return ids;
}

template <typename Table>
std::vector<dpx::TableId> getColumnVariables(dpx::TableId columnId, const Table& variables)
{
    std::vector<dpx::TableId> ids;
    findOne([&](dpx::TableId id, const typename Table::Type& tableEntry)
    {
        if(tableEntry.column == columnId)
                ids.push_back(id);

        return false;
    }, variables);

    return ids;
}

template <typename Table>
dpx::TableId idFromName(const std::string& name, const Table& table)
{
    auto result = findOne([&](dpx::TableId id, typename Table::Type& tableEntry)
    {
        return tableEntry.name == name;
    }, table);

    if(result)
    {
        return result->id;
    }
    else
    {
        throw NonExistingNameException(name);
    }
}

template <typename Table>
dpx::TableId idFromNameAndColumn(const std::string& name, dpx::TableId columnId, const Table& table)
{
    auto result = findOne([&](dpx::TableId id, typename Table::Type& tableEntry)
    {
        return tableEntry.name == name && tableEntry.column == columnId;
    }, table);

    if(result)
    {
        return result->id;
    }
    else
    {
        throw NonExistingNameException(name);
    }
}

template <typename Table>
dpx::TableId findIdFromNameAndColumn(const std::string& name, dpx::TableId columnId, const Table& table)
{
    auto result = findOne([&](dpx::TableId id, typename Table::Type& tableEntry)
    {
        return tableEntry.name == name && tableEntry.column == columnId;
    }, table);

    if(result)
    {
        return result->id;
    }
    else
    {
        return dpx::Null;
    }
}

std::vector<dpx::TableId> children(dpx::TableId parentId, const ProjectData& data);

std::string generateColumnOutput(dpx::TableId columnId, const ProjectData& projectData);
