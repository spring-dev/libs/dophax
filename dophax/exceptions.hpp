#pragma once
#include <stdexcept>
#include <string>

class InvalidFileException : public std::runtime_error
{
    public:
        enum Mode { Read, Write };
          InvalidFileException(Mode mode, const std::string& filePath)
              : runtime_error(mode == Read ? ("Could not read to file: " + filePath) : ("Could not write to file: " + filePath))
          {
          }
};

class ProjectParseException : public std::runtime_error
{
    public:
          ProjectParseException(const std::string& filePath)
              : runtime_error("Failed to parse " + filePath)
          {}
};

class NonExistingNameException : public std::runtime_error
{
    public:
          NonExistingNameException(const std::string& name)
              : runtime_error("Table does not contain sought after name " + name)
          {
          }
};

class VariableParseException : public std::runtime_error
{
    public:
          VariableParseException(const std::string& variableString)
              : runtime_error("Could not parse variable string: " + variableString)
          {
          }
};

class UnclosedTagException : public std::runtime_error
{
    public:
          UnclosedTagException(int32_t lineNumber, const std::string& tag)
              : runtime_error("Unclosed '" + tag + "' tag on line " + std::to_string(lineNumber))
          {
          }
};

class UnexpectedEndException : public std::runtime_error
{
    public:
          UnexpectedEndException()
              : runtime_error("Reached end of template with unclosed tags. Forgot a @end@?")
          {
          }
};

class TemplateException : public std::runtime_error
{
    public:
          TemplateException(const std::string& text)
              : runtime_error(text)
          {
          }
};
