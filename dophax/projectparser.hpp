#pragma once
#include <string>
#include "projectdata.hpp"

ProjectData loadProjectData(const std::string& filePath);
void saveProjectData(const std::string& filePath, const ProjectData& projectData);
