#include "projectdata.hpp"

std::vector<dpx::TableId> children(dpx::TableId parentId, const ProjectData& data)
{
    std::vector<dpx::TableId> result;

    forEach([&] (dpx::TableId tableId, const Table& table)
    {
        if(table.inherits && *table.inherits == parentId)
            result.push_back(tableId);
    }, data.tTable);

    return result;
}

std::string generateColumnOutput(dpx::TableId columnId, const ProjectData& projectData)
{
    const TableColumn& column = get(columnId, projectData.tTableColumn);
    const DataType& dataType = get(column.dataType, projectData.tDataType);
    const FieldType& fieldType = get(column.fieldType, projectData.tFieldType);

    std::string result = fieldType.output;

    while(result.find_first_of('|') != std::string::npos)
    {
        size_t start = result.find_first_of('|');
        size_t end = result.find_first_of('|', start + 1);
        std::string variableString = result.substr(start + 1, end - start - 1);

        std::string replaceWith;

        if(variableString == "type")
            replaceWith = dataType.output;
        else if(variableString == "name")
            replaceWith = column.name;
        else
        {
            std::string variableName = variableString.substr(2);
            dpx::TableId id = findIdFromNameAndColumn(variableName, columnId, projectData.tFieldTypeVariable);
            if(id != dpx::Null)
            {
                const FieldTypeVariable& var = get(id, projectData.tFieldTypeVariable);
                replaceWith = var.value;
            }
            else
                replaceWith = "NULL";
        }

        result.replace(start, end - start + 1, replaceWith);
    }

    return result;
}
