$.ajaxSetup({async:false});

let data = {};
let dataSegmentId = {};

let undoBuffers = {};
let redoBuffers = {};

if (!Array.prototype.last){
    Array.prototype.last = function(){
        return this[this.length - 1];
    };
};

function generateTypeList(data)
{
	let header = $("#page-header");
	header.empty();

	header.append('<div class="header-buttons-spacer"/>');
	header.append('<div class="field-name">Name</div>');
	header.append('<div class="field-output">Output</div>');

	let parent = $("#page-list");

	$.each(data, function() 
	{
		let dataType = this;

		let entryParent = $('<div class="entry-parent"/>');
		entryParent.appendTo(parent);
		entryParent.append('<div class="row-handle"><i class="fas fa-ellipsis-v drag-handle"/></div>');

		let contentDiv = $('<div class="entry-content"/>');
		entryParent.append(contentDiv);

		let nameDiv = $('<div class="field-name"><textarea></textarea></div>');
		nameDiv.change(onChange);
		nameDiv.children().val(dataType["name"]);
		nameDiv.appendTo(contentDiv);

		let outputDiv = $('<div class="field-output"><textarea></textarea></div>');
		outputDiv.change(onChange);
		outputDiv.children().val(dataType["output"]);
		outputDiv.appendTo(contentDiv);

		let dependenciesHeaderDiv = $('<div class="sub-entry-header">Dependencies</div>');
		dependenciesHeaderDiv.appendTo(contentDiv);
		dependenciesHeaderDiv.click(() => dependenciesHeaderDiv.toggleClass("header-open"));

		let depRow = $('<div class="sub-entry-parent"/>');
		depRow.appendTo(contentDiv);

		let createRow = function(dep)
		{
			let parent = $('<div class="sub-entry-row"></div>');
			createDeleteDuplicateButtons(parent);

			let subEntryDiv = $('<textarea class="field-dependencies"></textarea>');
			subEntryDiv.change(onChange);
			subEntryDiv.val(dep);
			parent.append(subEntryDiv);

			parent.appendTo(depRow);

			return parent;
		};

		$.each(dataType["dependencies"], function() 
		{
			createRow(this);
		});
	});
}

function deleteButtonFunction()
{
	let parent = $(this).parent();
	let inputDiv = parent.children("textarea");
	if(parent.parent().children("div").length == 1)
	{
		inputDiv.val("");
	}
	else
	{
		parent.remove();
	}
	onChange();
}

function duplicateButtonFunction()
{
	let parent = $(this).parent();
	parent.after(parent.clone(true));
	onChange();
}

function createDeleteDuplicateButtons(parent)
{
	let delButton = $('<i class="fa fa-trash"></i>');
	let dupButton = $('<i class="fa fa-clone"></i>');
	parent.append(delButton);
	parent.append(dupButton);
	delButton.click(deleteButtonFunction);
	dupButton.click(duplicateButtonFunction);
}

function generateTablePage(tableData)
{
	let header = $("#page-header");
	header.empty();

	header.append('<div class="header-buttons-spacer"/>');
	header.append('<div class="field-name">Name</div>');
	header.append('<div class="field-external-id">External Id</div>');
	header.append('<div class="field-inherits">Inherits</div>');
	header.append('<div class="field-description">Description</div>');

	let parent = $("#page-list");

	$.each(tableData, function() 
	{
		let entryData = this;

		let entryParent = $('<div class="entry-parent"/>');
		entryParent.appendTo(parent);
		entryParent.append('<div class="row-handle"><i class="fas fa-ellipsis-v drag-handle"/></div>');

		let contentDiv = $('<div class="entry-content"/>');
		entryParent.append(contentDiv);

		let createField = function(fieldId)
		{
			let fieldDiv = $('<div class="field-' + fieldId.replace("_", "-") + '"></div>');

			if(typeof entryData[fieldId] == "boolean")
			{
				fieldDiv.append("<input type='checkbox'/>");
				fieldDiv.children()[0].checked  = entryData[fieldId];
			}
			else if(typeof entryData[fieldId] == "string" || true)
			{
				fieldDiv.append("<textarea/>");
				fieldDiv.children().val(entryData[fieldId]);
			}

			fieldDiv.change(onChange);
			fieldDiv.appendTo(contentDiv);
		};

		createField("name");
		createField("external_id");
		createField("inherits");

		let createSubrows = function(subEntryType)
		{
			let subEntryHeader = $('<div class="sub-entry-header">' + subEntryType + '</div>');
			subEntryHeader.appendTo(contentDiv);
			subEntryHeader.click(() => subEntryHeader.toggleClass("header-open"));

			let subEntryParent = $('<div class="sub-entry-parent"/>');
			subEntryParent.appendTo(contentDiv);

			if(!entryData[subEntryType] || entryData[subEntryType].length == 0)
			{
				entryData[subEntryType] = [""];
			}

			$.each(entryData[subEntryType], function() 
			{
				let subEntryData = this;

				let parent = $('<div class="sub-entry-row"></div>');
				createDeleteDuplicateButtons(parent);

				let subEntryDiv = $('<textarea class="field-' + subEntryType + '"></textarea>');
				subEntryDiv.change(onChange);
				subEntryDiv.val(subEntryData);
				parent.append(subEntryDiv);

				parent.appendTo(subEntryParent);

				return parent;
			});
		};

		createSubrows("dependencies");
		createSubrows("tags");

		let columnsHeader = $('<div class="sub-entry-header columns-header"></div>');
		columnsHeader.appendTo(contentDiv);
		columnsHeader.click(() => columnsHeader.toggleClass("header-open"));

		columnsHeader.append('<div class="subrow-header-title">Columns</div>')
		columnsHeader.append('<div class="subrow-header-spacer"></div>')
		columnsHeader.append('<div class="subrow-header-field">Name</div>')
		columnsHeader.append('<div class="subrow-header-field">Data Type</div>')
		columnsHeader.append('<div class="subrow-header-field">Field Type</div>')
		columnsHeader.append('<div class="subrow-header-field">Field Type</div>')

		let columnsParent = $('<div class="sub-entry-parent columns-rows"/>');
		columnsParent.appendTo(contentDiv);

		if(!entryData["columns"] || entryData["columns"].length == 0)
		{
			entryData["columns"] = [""];
		}

		$.each(entryData["columns"], function() 
		{
			let subEntryData = this;

			let parent = $('<div class="sub-entry-row"></div>');
			createDeleteDuplicateButtons(parent);

			let nameDiv = $('<textarea class="data-column-name"></textarea>');
			nameDiv.change(onChange);
			nameDiv.val(subEntryData["name"]);
			parent.append(nameDiv);

			let dataTypeDiv = $(`<textarea class="data-column-data-type"></textarea>'`);
			parent.append(dataTypeDiv);
			dataTypeDiv.change(onChange);
			dataTypeDiv.val(subEntryData["data_type"]);
			dataTypeDiv.data("options", function()
			{
				let options = [];

				for(let dataType of data["data_types"])
				{
					options.push(dataType.name);
				}

				return options;
			});
			dataTypeDiv.combobox();

			let fieldTypeDiv = $(`<textarea class="data-column-field-type"></textarea>'`);
			parent.append(fieldTypeDiv);
			fieldTypeDiv.change(onChange);
			fieldTypeDiv.val(subEntryData["field_type"]);
			fieldTypeDiv.data("options", function()
			{
				let options = [];

				for(let dataType of data["field_types"])
				{
					options.push(dataType.name);
				}

				return options;
			});
			fieldTypeDiv.combobox();

			let variablesHolder = $('<div class="variables-holder"></div>');
			variablesHolder.change(onChange);
			variablesHolder.val(subEntryData);
			parent.append(variablesHolder);

			let variablesBin = $('<div class="variables-bin"></div>');
			variablesBin.change(onChange);
			variablesBin.val(subEntryData);
			variablesHolder.append(variablesBin);

			let variablesHeader = $('<div class="variables-header"></div>');
			variablesHeader.appendTo(variablesBin);
			variablesHeader.append('<div class="subrow-header-spacer"></div>');
			variablesHeader.append('<div class="variables-header-name">Name</div>');
			variablesHeader.append('<div class="variables-header-value">Value</div>');

			if(!subEntryData["field_type_variables"] || subEntryData["field_type_variables"].length == 0)
			{
				subEntryData["field_type_variables"] = [{}];
			}

			$.each(subEntryData["field_type_variables"], function() 
			{
				let variableData = this;
				let variablesRow = $('<div class="variables-row"></div>');
				variablesRow.appendTo(variablesBin);
				createDeleteDuplicateButtons(variablesRow);

				let nameDiv = $('<textarea class="data-variables-name"></textarea>');
				nameDiv.change(onChange);
				nameDiv.val(variableData["name"]);
				variablesRow.append(nameDiv);

				let valueDiv = $('<textarea class="data-variables-value"></textarea>');
				valueDiv.change(onChange);
				valueDiv.val(variableData["value"]);
				variablesRow.append(valueDiv);
			});
			
			let variablesDiv = $('<div class="variables-checkbox"></div>');
			variablesDiv.click(function()
			{
				$(".variables-holder").not(variablesHolder).removeClass("variables-visible");;
				variablesHolder.toggleClass("variables-visible");
			});
			parent.append(variablesDiv);

			parent.appendTo(columnsParent);

			return parent;
		});


		let descriptionDiv = $('<div class="field-description"><textarea></textarea></div>');
		descriptionDiv.change(onChange);
		descriptionDiv.children().val(entryData["description"]);
		descriptionDiv.appendTo(entryParent);
	});
}

function generatePage()
{
	console.log(data);

	if(currentPage == "data-types" || currentPage == "field-types")
	{
		generateTypeList(data[dataSegmentId]);
	}
	else if(currentPage == "tables")
	{
		generateTablePage(data[dataSegmentId])
	}
	else if(currentPage == "settings")
	{
	}
}

let filePath = "";
let autoSave = false;
$(document).ready(function()
{
	autoSave = getCookie("autoSave");
	$('#menu-header-auto-save').prop('checked', autoSave);

	$('#menu-header-auto-save').change(function ()
	{
	    autoSave = $(this).is(':checked');
		setCookie("autoSave", autoSave, 99999);
	});

    selectPage('tables');

	$("#page-list").sortable(
	{
		axis: "y",
		cancel: "input,textarea,button,select,option,.no-drag",
		handle: ".drag-handle",
		start: () => $("#page-interactions").show(),
 		stop: function(ev, ui)
 		{
 			if($("#page-interactions > .fa-clone:hover").length)
 			{
 				$("#page-list").sortable("cancel");
 				ui.item.after(ui.item.clone(true));
 			}
 			else if($("#page-interactions > .fa-trash:hover").length)
 			{
 				$("#page-list").sortable("cancel");
 				ui.item.remove();
 			}
 			$("#page-interactions").hide();

 			onChange();
 		},
	});

	load();
})

function serializeData()
{
	let dataSegment = (data[dataSegmentId] = []);

	if(currentPage == "data-types" || currentPage == "field-types")
	{
		$(".entry-parent").not(".ui-sortable-placeholder").each(function()
		{
			let div = $(this);
			let elem = {};

			elem["name"] = div.find(".field-name > textarea").val();
			elem["output"] = div.find(".field-output > textarea").val();

			elem["dependencies"] = [];
			div.find(".field-dependencies").each(function()
			{
				elem["dependencies"].push($(this).val());
			});

			dataSegment.push(elem);
		});
	}
	else if(currentPage == "tables")
	{
		$(".entry-parent").not(".ui-sortable-placeholder").each(function()
		{
			let div = $(this);
			dataSegment.push({});
			let elem = dataSegment.last();

			elem["name"] = div.find(".field-name > textarea").val();
			elem["external_id"] = div.find(".field-external-id > input")[0].checked;
			elem["inherits"] = div.find(".field-inherits > textarea").val() || null;
			elem["description"] = div.find(".field-description > textarea").val();

			elem["dependencies"] = [];
			div.find(".field-dependencies").each(function()
			{
				elem["dependencies"].push($(this).val());
			});

			elem["tags"] = [];
			div.find(".field-tags").each(function()
			{
				elem["tags"].push($(this).val());
			});

			elem["columns"] = [];
			div.find(".columns-rows .sub-entry-row").each(function()
			{
				let parent = $(this);
				elem["columns"].push({});
				let columnData = elem["columns"].last();

				columnData["name"] = parent.find(".data-column-name").val();;
				columnData["data_type"] = parent.find(".data-column-data-type").val();
				columnData["field_type"] = parent.find(".data-column-field-type").val();

				columnData["field_type_variables"] = [];

				parent.find(".variables-row").each(function()
				{
					let parent = $(this);
					columnData["field_type_variables"].push({});
					let variableData = columnData["field_type_variables"].last();

					variableData["name"] = parent.find(".data-variables-name").val();
					variableData["value"] = parent.find(".data-variables-value").val();
				});
			});
		});
	}

	console.log(dataSegment);

	return dataSegment;
}

function onChange()
{
	redoBuffers[dataSegmentId] = [];
	bufferChanges();
}

function undo()
{
	$(":focus").blur();

	let redoBuffer = redoBuffers[dataSegmentId];
	let undoBuffer = undoBuffers[dataSegmentId];

	if(undoBuffer.length > 1)
	{
		redoBuffer.push(undoBuffer.pop());
		data[dataSegmentId] = JSON.parse(undoBuffer[undoBuffer.length - 1]);
		generatePage();
	}

	bufferChanges(false);
}

function redo()
{
	if($(":focus").length)
	{
		return;
	}

	let redoBuffer = redoBuffers[dataSegmentId];
	let undoBuffer = undoBuffers[dataSegmentId];

	if(redoBuffer.length > 0)
	{
		undoBuffer.push(redoBuffer[redoBuffer.length - 1]);
		data[dataSegmentId] = JSON.parse(redoBuffer.pop());
		generatePage();
	}

	bufferChanges(false);
}

function bufferChanges(addUndo = true)
{
	let undoBuffer = undoBuffers[dataSegmentId];

	let dataSegment = serializeData();
	let dataSegmentStr = JSON.stringify(dataSegment, null, 2);

	if(addUndo)
	{
		undoBuffer.push(dataSegmentStr);
	}

	if(autoSave)
	{
		save();
	}
}

function load(newFilePath)
{
	if(!newFilePath)
	{
		newFilePath = getCookie("filePath");
		if(!newFilePath)
		{
			$.get('/local_path', function(rawData)
			{
				rawData = rawData.replace(/\\/g, '/');
				newFilePath = rawData + "/";
			});
		}
	}

	$.get('/load/' + newFilePath, function(rawData)
	{
		if(rawData == "invalid")
		{
			alert("Failed to load file");
			return
		}

		try
		{
			data = JSON.parse(rawData);
		}
		catch(e)
		{
			alert("Failed to load file");
			return
		}
	});


	filePath = newFilePath;
	setCookie("filePath", filePath, 99999);

	selectPage(currentPage);
}

function save()
{
	let dataStr = JSON.stringify(data, null, 2);

	$.post('/save/' + filePath, dataStr, function(answer)
	{
		if(answer == "invalid")
		{
			alert("Failed ot save file");
		}
	});
}

$(document).keydown(function (e) 
{
    if (e.keyCode == 90 && e.ctrlKey && e.shiftKey) 
    {
    	e.preventDefault()
    	redo();
    }
    else if (e.keyCode == 90 && e.ctrlKey) 
    {
    	e.preventDefault()
    	undo();
    }
});

let currentPage = "";
function selectPage(pageId)
{
	currentPage = pageId;

	if(currentPage == "data-types")
	{
		dataSegmentId = "data_types";
	}
	else if(currentPage == "field-types")
	{
		dataSegmentId = "field_types";
	}
	else if(currentPage == "tables")
	{
		dataSegmentId = "tables";
	}
	else if(currentPage == "settings")
	{
		dataSegmentId = "settings";
	}

	let parent = $("#page-list");
	parent[0].innerHTML = "";

	generatePage();

	if(!undoBuffers[dataSegmentId] || !redoBuffers[dataSegmentId])
	{
		undoBuffers[dataSegmentId] = [JSON.stringify(serializeData())];
		redoBuffers[dataSegmentId] = [];
	}

	$(".menu-header-page-selected").removeClass("menu-header-page-selected");
	$("#menu-header-" + pageId).addClass("menu-header-page-selected");
}