
$(document).ready(function()
{
	$("#file-browser-parent, #file-browser-background").hide();

	$("#file-browser-path").blur(onFileBrowserPathUpdated);
	$("#file-browser-path").keypress(function(e)
	{
        if(e.which == 13)
        {
        	e.preventDefault();
            onFileBrowserPathUpdated();
        }
    });
	
});

let lastFileBrowserPath = "/";
function onFileBrowserPathUpdated()
{
	let url = "";
	$.get("/enumerate_files/" + $("#file-browser-path").val(), function(rawData)
	{
		$("#file-browser-file-list").text(rawData);
		if(rawData == "invalid")
		{
			$("#file-browser-path").val(lastFileBrowserPath);
			onFileBrowserPathUpdated();
			return;
		}

		lastFileBrowserPath = $("#file-browser-path").val();

		$("#file-browser-file-list").empty();
		let data = JSON.parse(rawData);

		let onClick = function()
		{
			let path = $("#file-browser-path").val();
			let entryPath = $(this).text();

			if(path[path.length - 1] == "/")
			{
				path = path.substring(0, path.length - 1);
			}

			if(entryPath == "..")
			{
				path = path.substring(0, path.lastIndexOf("/")) + "/";
			}
			else
			{
				path += "/" + entryPath + "/";
			}

			$("#file-browser-path").val(path);
			onFileBrowserPathUpdated();
		};

		for(let dir of data.directories)
		{
			let entry = $("<div class='file-browser-dir'><i class='far fa-folder'></i>" + dir + "</div>");
			entry.click(onClick);
			$("#file-browser-file-list").append(entry);
		}

		for(let file of data.files)
		{
			let entry = $("<div class='file-browser-file'>" + file + "</div>");
			entry.click(function()
			{
				let path = $("#file-browser-path").val();
				let entryPath = $(this).text();

				if(path[path.length - 1] == "/")
				{
					path = path.substring(0, path.length - 1);
				}

				path += "/" + entryPath;

				closeFileBrowser();
				load(path);
			});

			$("#file-browser-file-list").append(entry);
		}
	});
}

function closeFileBrowser()
{
	$("#file-browser-parent, #file-browser-background").hide();
}

function openFileBrowser()
{
	let path = filePath;
	if(path)
	{
		path = path.substring(0, path.lastIndexOf("/")) + "/";
	}
	else
	{
		path = "/";
	}

	$("#file-browser-path").val(path);
	onFileBrowserPathUpdated();

	$("#file-browser-parent, #file-browser-background").show();
}