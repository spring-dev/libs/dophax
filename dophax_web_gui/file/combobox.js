$(function() 
{
	$.widget("custom.combobox", 
	{
		_create: function() 
		{
			this.wrapper = $("<span>")
				.addClass("custom-combobox")
				.insertAfter(this.element);

			this.element.detach().appendTo(this.wrapper)

			this._createAutocomplete();
			this._createShowAllButton();
		},

		_createAutocomplete: function() 
		{
			this.element
			.autocomplete(
			{
				delay: 0,
				minLength: 0,
				source: $.proxy(this, "_source"),
				change: onChange
			});
		},

		_createShowAllButton: function() 
		{
			var input = this.element;
			var wasOpen = false;

			$("<a>")
			.attr("tabIndex", -1)
			.appendTo(this.wrapper)
			.button({
				icons: 
				{
					primary: "ui-icon-triangle-1-s"
				},
				text: false
			})
			.removeClass("ui-corner-all")
			.addClass("custom-combobox-toggle ui-corner-right")
			.on("mousedown", function() {
				wasOpen = input.autocomplete("widget").is(":visible");
			})
			.on("click", function() 
			{
				input.trigger("focus");

				if (wasOpen) 
				{
					return;
				}

				input.autocomplete("search", "");
			});
		},

		_source: function(request, response) 
		{
			var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");

			let data = [];

			for(let text of this.element.data("options")())
			{
				if (!request.term || matcher.test(text))
				{
					data.push({
						label: text,
						value: text,
						option: this
					});
				}
			}

			response(data);
		},

		_destroy: function() 
		{
			this.wrapper.remove();
			this.element.show();
		}
	});
});