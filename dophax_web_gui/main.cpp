#include <fstream>
#include <unordered_map>
#include <iostream>

#ifdef SIMPLESERVER_EMBEDDED
#include "embedding.hpp"
#endif

#include "argagg.hpp"
#include "openurl.hpp"

#ifdef _WIN32
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem::v1;
#else
#include <filesystem>
namespace fs = std::filesystem;
#endif

#include "simpleserver.hpp"
using namespace SimpleServer;

std::string filesRoot = "./file";

std::string removeAll(std::string str, const std::string& toRemove)
{
    std::size_t pos = str.find(toRemove);
    while (pos != std::string::npos)
    {
        str.erase(pos, toRemove.size());
        pos = str.find(toRemove, pos);
    }

    return str;
}

std::unordered_map<std::string, std::string> extensionToMimeType =
{
    {"aac", "audio/aac"},
    {"abw", "application/x-abiword"},
    {"arc", "application/octet-stream"},
    {"avi", "video/x-msvideo"},
    {"azw", "application/vnd.amazon.ebook"},
    {"bin", "application/octet-stream"},
    {"bmp", "image/bmp"},
    {"bz", "application/x-bzip"},
    {"bz2", "application/x-bzip2"},
    {"csh", "application/x-csh"},
    {"css", "text/css"},
    {"csv", "text/csv"},
    {"doc", "application/msword"},
    {"docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
    {"eot", "application/vnd.ms-fontobject"},
    {"epub", "application/epub+zip"},
    {"gif", "image/gif"},
    {"htm", "text/html"},
    {"ico", "image/x-icon"},
    {"ics", "text/calendar"},
    {"jar", "application/java-archive"},
    {"jpeg", "image/jpeg"},
    {"js", "text/javascript"},
    {"json", "application/json"},
    {"mid", "audio/x-midi"},
    {"mjs", "text/javascript"},
    {"mp3", "audio/mpeg"},
    {"mpeg", "video/mpeg"},
    {"mpkg", "application/vnd.apple.installer+xml"},
    {"odp", "application/vnd.oasis.opendocument.presentation"},
    {"ods", "application/vnd.oasis.opendocument.spreadsheet"},
    {"odt", "application/vnd.oasis.opendocument.text"},
    {"oga", "audio/ogg"},
    {"ogv", "video/ogg"},
    {"ogx", "application/ogg"},
    {"otf", "font/otf"},
    {"png", "image/png"},
    {"pdf", "application/pdf"},
    {"ppt", "application/vnd.ms-powerpoint"},
    {"pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
    {"rar", "application/x-rar-compressed"},
    {"rtf", "application/rtf"},
    {"sh", "application/x-sh"},
    {"svg", "image/svg+xml"},
    {"swf", "application/x-shockwave-flash"},
    {"tar", "application/x-tar"},
    {"tif", "image/tiff"},
    {"ts", "application/typescript"},
    {"ttf", "font/ttf"},
    {"txt", "text/plain"},
    {"vsd", "application/vnd.visio"},
    {"wav", "audio/wav"},
    {"weba", "audio/webm"},
    {"webm", "video/webm"},
    {"webp", "image/webp"},
    {"woff", "font/woff"},
    {"woff2", "font/woff2"},
    {"xhtml", "application/xhtml+xml"},
    {"xls", "application/vnd.ms-excel"},
    {"xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
    {"xml", "text/xml"},
    {"xul", "application/vnd.mozilla.xul+xml"},
    {"zip", "application/zip"},
    {"7z", "application/x-7z-compressed"},
};

#ifdef SIMPLESERVER_EMBEDDED
HttpRequest serveEmbeddedFile(std::string path)
{
    HttpRequest response;

    if(path.size() && path[0] == '/')
    {
        path.erase(0, 1);
    }

    auto it = embeddedFiles.find(path);
    if (it == embeddedFiles.end())
    {
        response.status = S_404;
        return response;
    }

    const auto& file = it->second;
    const char* str = reinterpret_cast<const char*>(file.data);
    response.body = std::string(str, file.size);

    std::string contentType = "text/plain";

    size_t extensionPos = path.find_last_of(".");
    if (extensionPos != std::string::npos)
    {
        const std::string extention = path.substr(extensionPos + 1);
        contentType = extensionToMimeType[extention];
    }

    response.headers["content-type"] = contentType;
    response.status = S_200;

    return response;
}
#endif

HttpRequest serveFilesystemFile(const std::string& path)
{
    std::cout << path << std::endl;

    HttpRequest response;
    std::ifstream file(path, std::ifstream::binary);
    if (!file)
    {
        response.status = S_404;
        return response;
    }

    file.seekg(0, std::ios::end);
    std::streampos length(file.tellg());

    if (length)
    {
        file.seekg(0, std::ios::beg);
        response.body.resize(length);
        file.read(&response.body.front(), length);
    }

    std::string contentType = "text/plain";

    size_t extensionPos = path.find_last_of(".");
    if (extensionPos != std::string::npos)
    {
        const std::string extention = path.substr(extensionPos + 1);
        contentType = extensionToMimeType[extention];
    }

    response.headers["content-type"] = contentType;
    response.status = S_200;

    return response;
}


HttpRequest serveRessourceFile(const std::string& path)
{
#ifdef SIMPLESERVER_EMBEDDED
    const auto& response = serveEmbeddedFile(path);
    if(response.status != S_404)
    {
        return response;
    }
#endif

    return serveFilesystemFile(filesRoot + "/" + path);
}

std::string decodeUrl(std::string str)
{
    for (size_t x = 0; x < str.size(); x++)
    {
        if (str[x] == '%')
        {
            size_t index = x + 1;
            const std::string hexString(str.c_str(), x + 1, x + 3);
            const char c = std::stoi(hexString, 0, 16);
            str.replace(x, 3, &c, 1);
        }
    }

    return str;
}

HttpRequest process(HttpRequest& r)
{
    HttpRequest response;

    r.path = decodeUrl(r.path);

    std::cout << r.path << std::endl;

    auto path = split(r.path, "/", true);

    const std::string filePath("game.dpx");

    if (path.empty())
    {
        return serveRessourceFile("index.html");
    }
    else if (path[0] == "load")
    {
        const std::string filePath = r.path.substr(path[0].size() + 2);

        try
        {
            std::cout << "load: " << filePath << std::endl;
            return serveFilesystemFile(filePath);
        }
        catch (...)
        {
            std::cout << "!INVALID!" << std::endl;
            response.body = "invalid";
            return response;
        }
    }
    else if (path[0] == "save")
    {
        const std::string filePath = r.path.substr(path[0].size() + 2);

        try
        {
            std::cout << "save: " << filePath << std::endl;
            std::ofstream file(filePath, std::ifstream::binary);
            file << r.body;
        }
        catch (...)
        {
            std::cout << "!INVALID!" << std::endl;
            response.body = "invalid";
            return response;
        }
    }
    else if (path[0] == "local_path")
    {
        response.body = fs::current_path().string();
        return response;
    }
    else if (path[0] == "enumerate_files")
    {
        const std::string directoryPath = r.path.substr(path[0].size() + 2);
        std::cout << directoryPath << std::endl;

        try
        {
            std::string dirList = "\"..\"";
            std::string fileList;

            for (const auto& entry : fs::directory_iterator(directoryPath))
            {
                std::string& list = fs::is_directory(entry.status()) ? dirList : fileList;

                if (!list.empty())
                {
                    list += ", ";
                }

                list += "\"" + entry.path().filename().string() + "\"";
            }

            response.body = "{\n\"directories\":[" + dirList + "], \"files\":[" + fileList + "]\n}";
        }
        catch (...)
        {
            std::cout << "!INVALID!" << std::endl;
            response.body = "invalid";
            return response;
        }
    }
    else
    {
        return serveRessourceFile(r.path);
    }

    return response;
}

int main(int argc, const char** argv)
{
    argagg::parser argParser 
    {
        {
            { "help", {"-h", "--help"}, "shows this help message", 0},
            { "root", {"-r", "--root"}, "internal files path (default: './file')", 1},
            { "auto_open", {"-o", "--open"}, "open the page in the default browser when starting the server", 0},
        }
    };

    argagg::parser_results args;
    try
    {
        args = argParser.parse(argc, argv);
    } 
    catch (const std::exception& e) 
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    if(args["root"])
    {
        filesRoot = args["root"].as<std::string>();
    }

    sockInit();

    auto listener = makeListener(8080, false);
    if (SOCKET_IS_INVALID(listener))
        throw std::runtime_error("ERROR on makeListeneer");

    std::cout << "Running server on 127.0.0.1:8080" << std::endl;

    if(args["auto_open"])
    {
        openUrl("http://127.0.0.1:8080");
    }

    while (true)
    {
        auto client = makeSocket(listener);
        if (SOCKET_IS_INVALID(client))
            throw std::runtime_error("ERROR on makeSocket");

        auto request = HttpParser().receiveRequest(client);
        auto processed = process(request);
        auto response = finishResponse(processed);

        send(client, response.c_str(), response.size(), 0);

        sockClose(client);
    }

    sockClose(listener);

    sockQuit();

    return 0;
}
