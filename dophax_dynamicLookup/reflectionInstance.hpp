#pragma once

#include "registeredstruct.hpp"

class ReflectionInstance
{
public:

    struct MemberInstance
    {
        std::type_index type;
        void* ptr;
    };

    template <typename T>
    ReflectionInstance(const Reflection& reflection, T& data) : ReflectionInstance(reflection, (void*)&data)
    {
        assert(reflection.type() == typeid(T));
    }

    ReflectionInstance(const Reflection& reflection, void* data) : mType(reflection.type())
    {
        mType = reflection.type();

        for(auto& pair: reflection.members())
        {
            auto type = pair.second.type;
            auto ptr = pair.second.ptr->get(data);
            mMembers.emplace(pair.first, MemberInstance{type, ptr});
        }
    }

    void* get(const std::string& memberName)
    {
        return mMembers.at(memberName).ptr;
    }

    template <typename T>
    T& get(const std::string& memberName)
    {
        auto& member = mMembers.at(memberName);
        assert(member.type == typeid(T));
        return *(T*)member.ptr;
    }

    const void* get(const std::string& memberName) const
    {
        return mMembers.at(memberName).ptr;
    }

    template <typename T>
    const T& get(const std::string& memberName) const
    {
        const auto& member = mMembers.at(memberName);
        assert(member.type == typeid(T));
        return *(T*)member.ptr;
    }

    bool hasMember(const std::string& memberName) const
    {
        return mMembers.find(memberName) != mMembers.end();
    }

    const MemberInstance& member(const std::string& memberName)
    {
        return mMembers.at(memberName);
    }

    std::type_index memberType(const std::string& memberName) const
    {
        return mMembers.at(memberName).type;
    }

    const std::unordered_map<std::string, MemberInstance>& members() const
    {
        return mMembers;
    }

    std::type_index type() const
    {
        return mType;
    }

//    void* mData;
    std::type_index mType;
    std::unordered_map<std::string, MemberInstance> mMembers;
};
