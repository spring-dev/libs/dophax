#include "../json/src/json.hpp"

using namespace nlohmann;

using JSONER = std::function<json(const void*)>;
using UNJSONER = std::function<void(json, void*)>;

static HttpRequest process(DynamicTableWrapper& wrapper, HttpRequest& request)
{
    const static std::unordered_map<std::type_index, JSONER> jsoners =
    {
        {typeid(int), [](auto data){return json(*(int*)data);}},
        {typeid(float), [](auto data){return json(*(float*)data);}},
        {typeid(std::string), [](auto data){return json(*(std::string*)data);}},
    };

    const static std::unordered_map<std::type_index, UNJSONER> unjsoners =
    {
        {typeid(int), [](auto j, void* data){*(int*)data = j;}},
        {typeid(float), [](auto j, void* data){*(float*)data = j;}},
        {typeid(std::string), [](auto j, void* data){*(std::string*)data = j;}},
    };

    HttpRequest response;

    if(request.path == "/poster")
    {
        std::ifstream file("dophax_dynamicLookup/poster.html", std::ifstream::binary);
        if(!file)
            return response;

        file.seekg(0, std::ios::end);
        std::streampos length(file.tellg());

        if(length > 0)
        {
            file.seekg(0, std::ios::beg);
            response.body.resize(length);
            file.read(&response.body[0], length);
        }

        return response;
    }

    json j;

    auto paths = split(request.path, "/", true);
    if(paths[0] == "tables" && paths.size() == 1)
    {
        std::vector<std::string> names;
        for(const auto& pair : wrapper.tables)
            names.push_back(pair.first);

        j = names;
    }
    else if(paths[0] == "tables" && paths.size() == 3 && paths[2] == "ids" )
    {
        std::vector<int32_t> ids;
        wrapper[paths[1]].forEach([&](auto id, auto){ids.push_back(id);});

        j = ids;
    }
    else if(paths[0] == "tables" && paths.size() == 4 && paths[2] == "erase" )
    {
        int32_t id = -1;
        id = std::stoi(paths[3]);

        auto& table = wrapper[paths[1]];

        table.erase(id);
    }
    else if(paths[0] == "tables" && (paths.size() == 3 || paths.size() == 4) && paths[2] == "insert" )
    {
        int32_t id = -1;
        if(paths.size() == 4)
            id = std::stoi(paths[3]);

        auto& table = wrapper[paths[1]];

        table.insert(nullptr, id);
    }
    else if(paths[0] == "tables" && paths.size() > 1 && request.method == GET)
    {
        auto tableName = paths[1];
        paths.erase(paths.begin(), paths.begin() + 2);

        int32_t index = -1;
        if(!paths.empty())
        {
            index = std::stoi(paths[0]);
            paths.erase(paths.begin());
        }

        auto& table = wrapper[tableName];

        std::function<json(const Reflection& reflection, const void* data)>
        jsonize = [&](const Reflection& reflection, const void* data)
        {
            std::string name = "";

            if(!paths.empty())
            {
                name = paths.front();
                paths.erase(paths.begin());
            }

            json j;

            if(!name.empty())
            {
                const auto& member = reflection.member(name);

                if(wrapper.hasType(member.type))
                {
                    json& jj = paths.empty() ? j[name] : j;
                    jj = jsonize(wrapper.getReflection(member.type), reflection.get(name, data));
                }
                else
                {
                    j[name] = jsoners.at(member.type)(reflection.get(name, data));
                }
            }
            else
            {
                for(const auto& pair : reflection.members())
                {
                    const auto& name = pair.first;
                    const auto& member = pair.second;

                    if(wrapper.hasType(member.type))
                    {
                        json& jj = paths.empty() ? j[name] : j;
                        jj = jsonize(wrapper.getReflection(member.type), member.get(data));
                    }
                    else
                    {
                        j[name] = jsoners.at(member.type)(member.get(data));
                    }
                }
            }

            return j;
        };

        auto lambda = [&](int32_t id, void* value)
        {
            json& jj = paths.empty() && index == -1 ? j[tableName][std::to_string(id)] : j;

            auto type = table.type;
            if(wrapper.hasType(type))
            {
                jj = jsonize(wrapper.getReflection(type), value);
            }
            else
                jj = jsoners.at(table.type)(value);
        };

        if(index == -1)
            table.forEach(lambda);
        else
            lambda(index, table.get(index));
    }
    else if(paths[0] == "tables" && paths.size() > 1 && request.method == POST)
    {
        auto tableName = paths[1];
        paths.erase(paths.begin(), paths.begin() + 2);

        int32_t index = -1;
        if(!paths.empty())
        {
            index= std::stoi(paths[0]);
            paths.erase(paths.begin());
        }

        auto& table = wrapper[tableName];
        j = json::parse(request.body);

        std::function<void(Reflection, void*, json&)>
        jsonize = [&](Reflection reflection, void* data, json& j)
        {
            std::string path = "";

            if(paths.size())
            {
                path = paths.front();
                paths.erase(paths.begin());
            }

            if(paths.size() && path.size())
            {
                for(auto& member : reflection.members())
                {
                    auto type = member.second.type;
                    auto name = member.first;

                    if(!path.empty() && path != name)
                        continue;

                    if(wrapper.hasType(type))
                        jsonize(wrapper.getReflection(type), reflection.get(name, data), j);
                    else
                        assert(false);
                }
            }
            else
            {
                for(auto& member : reflection.members())
                {
                    auto type = member.second.type;
                    auto name = member.first;

                    if(j[name].is_null())
                        continue;

                    if(wrapper.hasType(type))
                        jsonize(wrapper.getReflection(type), reflection.get(name, data), j[name]);
                    else
                        unjsoners.at(type)(j[name], reflection.get(name, data));

                }
            }
        };

        auto lambda = [&](int32_t id, void* value)
        {
            auto jj = j;

            if(index == -1 && !j[std::to_string(id)].is_null())
                jj = j[std::to_string(id)];

            auto type = table.type;
            if(wrapper.hasType(type))
            {
                jsonize(wrapper.getReflection(type), value, jj);
            }
            else
                unjsoners.at(table.type)(jj, value);
        };

        if(index == -1)
            table.forEach(lambda);
        else
            lambda(index, table.get(index));
    }

    response.body = "<pre>" + j.dump(4) + "</pre>";
    std::cout << j.dump(4) << std::endl;

    return response;
}

static void server(DynamicTableWrapper& wrapper)
{
    sockInit();

    auto listener = makeListener(8080, false);
    if(SOCKET_IS_INVALID(listener))
        std::cout << "ERROR on listen" << std::endl;

    while(true)
    {
        auto client = makeSocket(listener);
        if (SOCKET_IS_INVALID(client))
            std::cout << "ERROR on accept" << std::endl;

        auto request = HttpParser().receiveRequest(client);
        auto processed = process(wrapper, request);
        auto response = createReponse(processed);

        send(client, response.c_str(), response.size(), 0);

        sockClose(client);
    }

    sockClose(listener);

    sockQuit();
}

