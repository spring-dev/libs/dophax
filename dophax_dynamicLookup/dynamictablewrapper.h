#pragma once

#include <unordered_map>
#include <sstream>

#include "../datatables/common.hpp"
#include "../datatables/get.hpp"
#include "../datatables/foreach.hpp"
#include "../datatables/insert.hpp"
#include "../datatables/erase.hpp"

#include "registeredstruct.hpp"
#include "reflector.hpp"

class DynamicTableWrapper : public Reflector
{
public:

    using Printer = std::function<std::string(const void*)>;
    using Setter = std::function<void(void*, const std::string&)>;

    typedef void* VoidPtr;
    struct TableRef
    {
        std::type_index type;
        std::function<void(int32_t id)> erase;
        std::function<void(void* data, int32_t id)> insert;
        std::function<VoidPtr(int32_t id)> get;
        std::function<void(std::function<void(int, void*)>)> forEach;
    };

    template <typename...Args>
    DynamicTableWrapper(Args&&...args)
    {
        addTable(args...);
    }

    template <typename T, typename Functor>
    void addPrinter(Functor printer)
    {
        printers[typeid(T)] =
        [&, printer](void* ptr)
        {
            return printer(*(T*)ptr);
        };
    }


    //TODO make it possible to accept multiple type
    template <typename T>
    void addPrinter()
    {
        printers[typeid(T)] =
        [&](const void* ptr)
        {
            std::stringstream ss;
            ss << *(T*)ptr;
            return ss.str();
        };
    }

    template <typename T, typename Functor>
    void addSetter(Functor setter)
    {
        setters[typeid(T)] =
        [&, setter](void* ptr, const std::string& s)
        {
            setter(*(T*)ptr, s);
        };
    }


    //TODO make it possible to accept multiple type
    template <typename T>
    void addSetter()
    {
        setters[typeid(T)] =
        [&](void* ptr, const std::string& s)
        {
            std::stringstream ss;
            ss << s;
            ss >> *(T*)ptr;
        };
    }

    void print(const Reflection& reflection, const void* data, std::string prefix)
    {
        for(auto& member : reflection.members())
        {
            auto type = member.second.type;
            auto name = member.first;

            auto currentPrefix = prefix + "." + name;

            if(hasType(type))
                print(getReflection(type), reflection.get(name, data), currentPrefix);
            else
                std::cout << currentPrefix << " = " << printers.at(type)(reflection.get(name, data)) << std::endl;
        }
    }

    void print(std::string tableName)
    {
        auto& table = tables.at(tableName);
        table.forEach([&](int32_t id, void* value)
        {
            auto type = table.type;
            if(hasType(type))
                print(getReflection(type), value, tableName + "[" + std::to_string(id) + "]");
            else
                std::cout << tableName << "[" << id << "] = " << printers.at(table.type)(value) << std::endl;

        });
    }

    template <typename Type, bool externalIds>
    struct makeInserter;

    template <typename Type>
    struct makeInserter<Type, true>
    {
        template <typename Table>
        static decltype(auto) inserter(Table& table)
        {
            return [&table](void* ptr, auto id)
            {
                assert(id != -1);

                std::shared_ptr<Type> sharedPtr;
                if(ptr == nullptr)
                {
                    sharedPtr = std::make_shared<Type>();
                    ptr = sharedPtr.get();
                }

                insert(id, *(Type*)ptr, table);
            };
        }
    };

    template <typename Type>
    struct makeInserter<Type, false>
    {
        template <typename Table>
        static decltype(auto) inserter(Table& table)
        {
            return [&table](void* ptr, auto id)
            {
                assert(id == -1);

                std::shared_ptr<Type> sharedPtr;
                if(ptr == nullptr)
                {
                    sharedPtr = std::make_shared<Type>();
                    ptr = sharedPtr.get();
                }

                insert(*(Type*)ptr, table);
            };
        }
    };

    template <typename Table, typename...Args>
    void addTable(const std::string& name, Table& table, Args&&...args)
    {
        using Type = typename Table::Type;

        tables.emplace(name,
        TableRef
        {
            typeid(typename Table::Type),
            [&](auto id){erase(id, table);},
            makeInserter<Type, Table::ExternalId::value>::inserter(table),
            [&](auto id) -> VoidPtr {return &get(id, table);},
            [&](auto functor)
            {
                forEach([&](auto id, auto& data)
                {
                    functor(id, (VoidPtr*)&data);
                }, table);
            }
        });

        addTable(args...);
    }

    void addTable() {};

    TableRef& operator[](const std::string& name)
    {
        return tables.at(name);
    }

    std::unordered_map<std::string, TableRef> tables;
    std::unordered_map<std::type_index, Printer> printers;
    std::unordered_map<std::type_index, Setter> setters;
};
