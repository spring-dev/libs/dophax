#include <vector>
#include <typeindex>
#include <assert.h>

#include "DataMemberWrapper.hpp"

#include "typeNameDemangle.hpp"
#include "../datatables/common.hpp"
#include "../datatables/insert.hpp"
#include "../datatables/get.hpp"
#include "dynamictablewrapper.h"
#include "../dophax_webinterface/server.hpp"
#include "server.hpp"

#include "reflector.hpp"
#include "registeredstruct.hpp"
#include "reflectionInstance.hpp"

struct Test3
{
    std::string v = "ba";
    std::string f = "ab";
};

struct Test2
{
  std::string e = "hej";
  int k = 9;
  Test3 l;
};

struct Test
{
  int a = 1;
  int b = 2;
  int c = 3;

  Test2 d;
  Test2 e;
  Test3 f;
};

void prompt(DynamicTableWrapper& wrapper)
{

    enum TokenType {MEMBER, ARRAY, ASSIGNEMENT, TABLE};
    const std::string tokenChars = ".[=";

    enum Commands {PRINT, LIST, SET, TYPE, PAUSE, SERVER, ERASE, INSERT, INVALID};
    const char* commandsStrings[] = {"print", "list", "set", "type", "pause", "server", "erase", "insert"};

    while(true)
    {
        std::cout << ">";

        std::string line;
        std::getline(std::cin, line);

        //to achieve persistent command (ie: you enter the command once and all the next
        //querry assume that the same command until a new oen is specified)
        //Move "Commands command = INVALID;" to before the while loop

        Commands command = INVALID;
        auto pos = line.find(" ");
        for(size_t x = 0; x < sizeof(commandsStrings) / sizeof(*commandsStrings); x++)
        {
            if(commandsStrings[x] == line.substr(0, pos))
            {
                command = (Commands)x;
                line.erase(0, pos);
                break;
            }
        }

        if(command == INVALID)
        {
            std::cout << "invalid command" << std::endl;
            std::cout << "fallback to print" << std::endl;

            command = PRINT;
        }

        if(command == PAUSE)
            while(1);

        if(command == SERVER)
            server(wrapper);


        //remove all spaces
        pos = 0;
        while((pos = line.find(" ", pos)) != std::string::npos)
            line.erase(pos, 1);

        if(line.empty())
            continue;

        //parse the line into tokens
        std::vector<std::pair<std::string, TokenType>> tokens;
        TokenType type = TABLE;
        pos = 0;
        size_t previousPos = pos;
        while(pos != std::string::npos)
        {
            pos = line.find_first_of(tokenChars, previousPos + 1);

            //if it's the first token then set type to table name
            if(previousPos == 0)
                type = TABLE;
            //else the type is detected using the delimiter (eg if it was separated by a dot then it's a member)
            else
            {
                type = (TokenType)tokenChars.find(line[previousPos]);
                previousPos++;
            }

            //end of line
            if(pos == std::string::npos)
            {
                auto token = line.substr(previousPos);
                tokens.emplace_back(token, type);
                break;
            }

            auto token = line.substr(previousPos, pos - previousPos);
            tokens.emplace_back(token, type);

            previousPos = pos;
        }


        if(command != SET && !tokens.empty() && tokens.back().second == ASSIGNEMENT)
        {
            std::cout << "assignement detected" << std::endl;
            std::cout << "fallback to set" << std::endl;

            command = SET;
        }

        if(command == PRINT)
        {
            if(tokens.empty())
            {
                std::cout << "print command needs at lest a table name" << std::endl;
                continue;
            }

            std::string tableName = tokens[0].first;
            tokens.erase(tokens.begin());

            int32_t index = -1;
            if(!tokens.empty() && tokens[0].second == ARRAY)
            {
                index = std::stoi(tokens[0].first);
                tokens.erase(tokens.begin());
            }

            auto& table = wrapper[tableName];

            std::function<void(const Reflection& reflection, const void* data, std::string prefix)>
             print = [&](const Reflection& reflection, const void* data, std::string prefix)
            {
                if(tokens.empty())
                {
                    wrapper.print(reflection, data, prefix);
                    return;
                }

                auto token = tokens.front();
                tokens.erase(tokens.begin());

                if(token.second != MEMBER)
                {
                    std::cout << token.first << "is not a member token" << std::endl;
                    return;
                }

                const auto& name = token.first;
                if(reflection.hasMember(name))
                {
                    auto& member = reflection.member(name);
                    auto& type = member.type;

                    auto currentPrefix = prefix + "." + name;

                    if(wrapper.hasType(type))
                        print(wrapper.getReflection(type), member.get(data), currentPrefix);
                    else
                        std::cout << currentPrefix << " = " << wrapper.printers[type](member.get(data)) << std::endl;
                }
                else
                {
                    std::cout << token.first << " is not a member of " << prefix << std::endl;
                }
            };

            auto lambda = [&](int32_t id, void* value)
            {
                auto type = table.type;
                if(wrapper.hasType(type))
                    print(wrapper.getReflection(type), value, tableName + "[" + std::to_string(id) + "]");
                else
                    std::cout << tableName << "[" << id << "] = " << wrapper.printers[table.type](value) << std::endl;
            };

            if(index == -1)
                table.forEach(lambda);
            else
                lambda(index, table.get(index));
        }
        else if(command == SET)
        {
            if(tokens.empty())
            {
                std::cout << "set command needs at least a table name and an index" << std::endl;
                continue;
            }

            std::string tableName = tokens[0].first;
            tokens.erase(tokens.begin());

            int32_t index = -1;
            if(!tokens.empty() && tokens[0].second == ARRAY)
            {
                index = std::stoi(tokens[0].first);
                tokens.erase(tokens.begin());
            }
            else
            {
                std::cout << "must provide a table index" << std::endl;
                continue;
            }

            if(tokens.empty() || tokens.back().second != ASSIGNEMENT)
            {
                std::cout << "last token must be an assignement" << std::endl;
                continue;

            }

            auto& table = wrapper.tables.at(tableName);

            std::function<void(Reflection reflection, void* data, std::string prefix)>
             set = [&](Reflection reflection, void* data, std::string prefix)
            {
                auto token = tokens.front();
                tokens.erase(tokens.begin());

                if(tokens.size() > 1 && token.second != MEMBER)
                {
                    std::cout << token.first << "is not a member token" << std::endl;
                    return;
                }

                const auto& name = token.first;
                if(reflection.hasMember(name))
                {
                    auto& member = reflection.member(name);
                    auto& type = member.type;

                    auto currentPrefix = prefix + "." + name;

                    if(tokens.size() == 1)
                        wrapper.setters[type](member.get(data), tokens.back().first);
                    else if(wrapper.hasType(type))
                        set(wrapper.getReflection(type), member.get(data), currentPrefix);
                    else
                    {
                        std::cout << "no relfection found for " << demangle(type.name()) << std::endl;
                    }
                }
                else
                {
                    std::cout << token.first << " is not a member of " << prefix << std::endl;
                }
            };

            auto& type = table.type;
            auto value = table.get(index);
            if(wrapper.hasType(type))
                set(wrapper.getReflection(type), value, tableName + "[" + std::to_string(index) + "]");
            else
                wrapper.setters[type](value, tokens.back().first);
        }
        else if(command == LIST)
        {
            if(tokens.empty())
            {
                std::cout << "list command needs at least a table name" << std::endl;
                continue;
            }

            std::string tableName = tokens[0].first;
            tokens.erase(tokens.begin());

            if(!tokens.empty() && tokens[0].second == ARRAY)
            {
                std::cout << "dont pass a index to list" << std::endl;
                continue;
            }

            auto& table = wrapper.tables.at(tableName);

            std::function<void(const Reflection& reflection, std::string prefix)>
             list = [&](const Reflection& reflection, std::string prefix)
            {
                auto token = tokens.front();
                tokens.erase(tokens.begin());

                if(token.second != MEMBER)
                {
                    std::cout << token.first << "is not a member token" << std::endl;
                    return;
                }

                const auto& name = token.first;
                if(reflection.hasMember(name))
                {
                    auto& member = reflection.member(name);
                    auto& type = member.type;

                    auto currentPrefix = prefix + "::" + name;

                    if(tokens.empty())
                        std::cout << "\"" << currentPrefix << "\" is of type \"" << demangle(type.name()) << "\"" << std::endl;
                    else if(wrapper.hasType(type))
                        list(wrapper.getReflection(type), currentPrefix);
                    else
                        std::cout << "no relfection found for " << demangle(type.name()) << std::endl;
                }
                else
                {
                    std::cout << token.first << " is not a member of " << prefix << std::endl;
                }
            };

            auto type = table.type;
            if(tokens.empty() || !wrapper.hasType(type))
            {
                std::cout << "\"" << tableName << "\" is a table of \"" << demangle(type.name()) << "\"" << std::endl;
            }
            else
                list(wrapper.getReflection(type), tableName);
        }
        else if(command == TYPE)
        {
            if(tokens.size() != 1)
            {
                std::cout << "Type command only takes one arg" << std::endl;
                continue;
            }

            for(const auto& reflection : wrapper.reflections)
            {
                if(demangle(reflection.first.name()) == tokens.back().first)
                {
                    std::cout << demangle(reflection.first.name()) << std::endl;
                    std::cout << "{" << std::endl;

                    for(const auto& member : reflection.second.members())
                        std::cout << "    " << demangle(member.second.type.name()) << " " << member.first << std::endl;

                    std::cout << "}" << std::endl;
                }
            }
        }
        else if(command == ERASE)
        {
            if(tokens.empty())
            {
                std::cout << "erase command needs a table name and an index" << std::endl;
                continue;
            }

            std::string tableName = tokens[0].first;
            tokens.erase(tokens.begin());

            int32_t index = -1;
            if(!tokens.empty() && tokens[0].second == ARRAY)
            {
                index = std::stoi(tokens[0].first);
                tokens.erase(tokens.begin());
            }
            else
            {
                std::cout << "erase command require an index" << std::endl;
                continue;
            }

            auto& table = wrapper.tables.at(tableName);
            table.erase(index);
        }
        else if(command == INSERT)
        {
            if(tokens.empty())
            {
                std::cout << "insert command needs a table name" << std::endl;
                continue;
            }

            std::string tableName = tokens[0].first;
            tokens.erase(tokens.begin());

            int32_t index = -1;
            if(!tokens.empty() && tokens[0].second == ARRAY)
            {
                index = std::stoi(tokens[0].first);
                tokens.erase(tokens.begin());
            }

            auto& table = wrapper.tables.at(tableName);

            table.insert(nullptr, index);
        }
    }
}

int main()
{
    struct TestStruct
    {
        std::string string = "hej";
        int i = 0;
        float f = 0;
    };

    Reflector reflector;
    reflector.reflect<TestStruct>("string", &TestStruct::string, "i", &TestStruct::i, "f", &TestStruct::f);

    Reflection reflection = reflector.getReflection<TestStruct>();
//    auto reflection = reflector.getReflection<TestStruct>(typeid(TestStruct));

    //Member is struct {type_index, memberWrapper}
    //members return pair<std::string name, Member>
    for(auto member : reflection.members())
        std::cout << member.first << std::endl; //prints members name

    TestStruct ts = {"HEJ", 99, 99.99};
//    ReflectionInstance instance = reflection.bind(ts);
    ReflectionInstance instance = reflector.makeInstance(ts);

    std::cout << instance.get<int>("i") << std::endl; // prints 99

//    return 0;

    DataTable<int, true> ints;
    DataTable<float, true> floats;
    DataTable<Test, true> test;
    DataTable<Test2, true> test2;

    DynamicTableWrapper wrapper("ints", ints, "floats", floats, "Test", test, "Test2", test2);

    insert(9, 999, ints);
    insert(10, 100, ints);

    insert(0, 0.0f, floats);
    insert(1, 1.1f, floats);
    insert(2, 2.2f, floats);
    insert(3, 3.3f, floats);

    insert(0, {}, test);

    insert(0, {"hej", 10, {"hej2", "hej3"}}, test2);
    insert(2, {"hell yeah", 42, {"hej2", "hej3"}}, test2);

    wrapper.reflect<Test>("a", &Test::a, "b", &Test::b, "c", &Test::c, "d", &Test::d, "e", &Test::e, "f", &Test::f);
    wrapper.reflect<Test2>("e", &Test2::e, "k", &Test2::k, "l", &Test2::l);
    wrapper.reflect<Test3>("f", &Test3::f, "v", &Test3::v);

//    wrapper.addPrinter<int>([](int& i){return std::to_string(i);});
//    wrapper.addPrinter<float>([](float& f){return std::to_string(f);});
//    wrapper.addPrinter<std::string>([](std::string& s){return s;});

    wrapper.addPrinter<int>();
    wrapper.addPrinter<float>();
    wrapper.addPrinter<std::string>();

    wrapper.addSetter<int>();
    wrapper.addSetter<float>();
    wrapper.addSetter<std::string>();

//    wrapper.print("ints");
//    std::cout << std::endl;
//    wrapper.print("floats");
//    std::cout << std::endl;
//    wrapper.print("Test2");
//    std::cout << std::endl;
//    wrapper.print("Test");

    prompt(wrapper);
}
