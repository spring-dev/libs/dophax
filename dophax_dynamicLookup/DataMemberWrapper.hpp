#pragma once

#include <memory>

class DataPtrWrapper
{
public:
    virtual ~DataPtrWrapper() = default;

    virtual const void* get(const void* classPtr) const = 0;
    virtual void* get(void* classPtr) = 0;

    virtual void set(const void* classPtr, const void* data) = 0;


    template <typename Class, typename Type>
    static std::shared_ptr<DataPtrWrapper> create(Type Class::* ptr)
    {
        return std::make_shared<DataPtrWrapper_impl<Class, Type>>(ptr);
    }

    typedef std::shared_ptr<DataPtrWrapper> shared_ptr;

private:
    template <typename Class, typename Type>
    class DataPtrWrapper_impl;
};

template <typename Class, typename Type>
class DataPtrWrapper::DataPtrWrapper_impl : public DataPtrWrapper
{
public:
    virtual ~DataPtrWrapper_impl() = default;

    DataPtrWrapper_impl(Type Class::* ptr) : mPtr(ptr){}

    const void* get(const void* classPtr) const override
    {
        const Type& t = ((Class*)classPtr)->*mPtr;
        return &t;
    }

    void* get(void* classPtr) override
    {
        Type& t = ((Class*)classPtr)->*mPtr;
        return &t;
    }

    void set(const void* classPtr, const void* data) override
    {
        Type& t = ((Class*)classPtr)->*mPtr;
        t = *(Type*)data;
    }

private:
    Type Class::* mPtr;
};
