#include <iostream>
#include <typeinfo>
#include <string>
#include <memory>
#include <cstring>

#if defined(__GNUG__) || defined(__clang__) // gnu C++ compiler and clang

#include <cxxabi.h>

static std::string demangle( const char* mangled_name ) {

    std::size_t len = 0 ;
    int status = 0 ;
    std::unique_ptr< char, decltype(&std::free) > ptr(
                __cxxabiv1::__cxa_demangle( mangled_name, nullptr, &len, &status ), &std::free ) ;
    return ptr.get() ;
}

#else

static std::string demangle( const char* name ) { return name ; }

#endif // defined(__GNUG__) || defined(__clang__)
