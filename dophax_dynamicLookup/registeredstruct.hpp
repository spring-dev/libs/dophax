#pragma once

class ReflectionInstance;

class Reflection
{
public:

    struct Member
    {
        std::type_index type;
        DataPtrWrapper::shared_ptr ptr;

        void* get(void* ptr) const
        {
            assert(ptr != nullptr);
            return (void*)this->ptr->get(ptr);
        }

        const void* get(const void* ptr) const
        {
            assert(ptr != nullptr);
            return (const void*)this->ptr->get(ptr);
        }

        template <typename T>
        T& get(void* ptr)
        {
            assert(type == typeid(T));
            return *(T*)get(ptr);
        }

        template <typename T>
        const T& get(const void* ptr) const
        {
            assert(type == typeid(T));
            return *(const T*)get(ptr);
        }
    };

    template <typename...Args>
    Reflection(Args&&... args)
    {
        registerMember(args...);
    }

    template <typename T>
    ReflectionInstance bind(T& data) const;
    ReflectionInstance bind(void* data) const;

    bool hasMember(const std::string memberName) const
    {
        return mMembers.find(memberName) != mMembers.end();
    }

    const Member& member(const std::string& memberName) const
    {
        return mMembers.at(memberName);
    }

    std::type_index memberType(const std::string& memberName) const
    {
        return mMembers.at(memberName).type;
    }

    void* get(std::string memberName, void* ptr)
    {
        assert(ptr != nullptr);
//        assert(hasMember(memberName));
        return (void*)mMembers.at(memberName).ptr->get(ptr);
    }

    const void* get(const std::string& memberName, const void* ptr) const
    {
        assert(ptr != nullptr);
//        assert(hasMember(memberName));
        return (const void*)mMembers.at(memberName).ptr->get(ptr);
    }

    template <typename T>
    T& get(const std::string& memberName, void* ptr)
    {
        auto& member = mMembers.at(memberName);
        assert(member.type == typeid(T));
        return *(T*)get(memberName, ptr);
    }

    template <typename T>
    const T& get(const std::string& memberName, const void* ptr) const
    {
        auto& member = mMembers.at(memberName);
        assert(member.type == typeid(T));
        return *(const T*)get(memberName, ptr);
    }

    const std::unordered_map<std::string, Member>& members() const
    {
        return mMembers;
    }

    std::type_index type() const
    {
        return mType;
    }

private:

    template <typename T, typename ClassType, typename...Args>
    void registerMember(std::string memberName, T ClassType::*ptr, Args... args)
    {
        if(mType == typeid(void))
            mType = typeid(ClassType);

        assert(mType == typeid(ClassType) && "member form multiple class");

        mMembers.emplace(memberName, Member{typeid(T), DataPtrWrapper::create(ptr)});
        registerMember(args...);
    }

    void registerMember(){};

    std::type_index mType = typeid(void);
    std::unordered_map<std::string, Member> mMembers;
};

#include "reflectionInstance.hpp"

template <typename T>
ReflectionInstance Reflection::bind(T& data) const
{
    return ReflectionInstance(*this, data);
}

ReflectionInstance Reflection::bind(void* data) const
{
    return ReflectionInstance(*this, data);
}

