#pragma once

#include "registeredstruct.hpp"
#include "reflectionInstance.hpp"

struct Reflector
{
    template <typename T, typename...Args>
    void reflect(Args&&...args)
    {
        reflections.emplace(typeid(T), Reflection{args...});
    }

    bool hasType(const std::type_index& type)
    {
        return reflections.find(type) != reflections.end();
    }

    const Reflection& getReflection(const std::type_index& type) const
    {
        return reflections.at(type);
    }

    template <typename T>
    const Reflection& getReflection() const
    {
        return reflections.at(typeid(T));
    }

    template <typename T>
    ReflectionInstance makeInstance(T& t)
    {
        return getReflection<T>().bind(t);
    }

    std::unordered_map<std::type_index, Reflection> reflections;
};
