#pragma once
#include <optional>
#include "set.hpp"
#include "erase.hpp"

namespace dpx
{
template <typename DataType, bool externalId>
DataType* assign(TableId id, std::optional<DataType> value, DataTable<DataType, externalId>& table)
{
    if(value)
        return &set(id, std::move(*value), table);
	else
	{
		erase(id, table);
		return nullptr;
	}
}
}
