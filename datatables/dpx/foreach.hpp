#pragma once
#include <type_traits>
#include "tables.hpp"

#pragma warning(disable: 4996)

namespace dpx
{

enum LoopResult { Continue, Break };

template <typename Functor, typename DataTable>
std::enable_if_t<std::is_same<std::result_of_t<Functor(TableId, typename DataTable::Type&)>, void>::value> forEach(Functor f, DataTable& table)
{
    for(size_t i = 0; i < table.ids.size(); ++i)
        f(table.ids[i], table.data[i]);
}

template <typename Functor, typename DataTable>
std::enable_if_t<std::is_same<std::result_of_t<Functor(typename DataTable::Type&)>, void>::value> forEach(Functor f, DataTable& table)
{
    for(size_t i = 0; i < table.data.size(); ++i)
        f(table.data[i]);
}

template <typename Functor, typename DataTable>
std::enable_if_t<std::is_same<std::result_of_t<Functor(TableId, typename DataTable::Type&)>, LoopResult>::value> forEach(Functor f, DataTable& table)
{
    for(size_t i = 0; i < table.ids.size(); ++i)
    {
        if(f(table.ids[i], table.data[i]) == LoopResult::Continue)
            continue;
        else
            break;
    }
}

}
