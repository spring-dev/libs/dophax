#pragma once
#include <string>
#include "tables.hpp"

template <typename Table>
std::string nameListEntries(Table&& table)
{
    return table.meta.name;
}

template <typename Table, typename...TailTables>
std::string nameListEntries(Table&& table, TailTables&&... tail)
{
    return table.meta.name + ", " + nameListEntries(tail...);
}

template <typename...Tables>
std::string tableListString(Tables&&... tables)
{
    return "[" + nameListEntries(tables...) + "]";
}
