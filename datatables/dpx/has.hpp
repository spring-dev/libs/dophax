#pragma once
#include <cstdint>
#include <algorithm>
#include "tables.hpp"
#include "sort.hpp"

namespace dpx
{

template <typename Table>
bool has(TableId id, const Table& table)
{
    return table.idIndices.find(id) != table.idIndices.end();
}

template <typename... IdContainer>
bool has(TableId id, const IdContainer&... idContainers)
{
    using swallow = int[];

    bool hasAll = true;

    (void)swallow{((hasAll &= (has(id, idContainers))), 0)...};
    return hasAll;
}

template <typename Functor, typename Table>
bool hasMatching(Functor f, const Table& table)
{
    auto idIter = table.ids.begin();
    auto dataIter = table.data.begin();

    for(; idIter != table.ids.end(); ++idIter, ++dataIter)
    {
        if(f(*idIter, *dataIter))
            return true;
    }

    return false;
}

template <typename Functor, typename... IdContainer>
bool hasMatching(Functor f, const IdContainer&... idContainers)
{
    using swallow = int[];

    bool hasAll = true;

    (void)swallow{((hasAll &= (hasMatching(f, idContainers))), 0)...};
    return hasAll;
}

}
