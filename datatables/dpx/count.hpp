#pragma once
#include "tables.hpp"

namespace dpx
{

template <typename DataTable>
int32_t count(DataTable& table)
{
    return static_cast<int32_t>(table.ids.size());
}

template <typename Functor, typename DataTable>
int32_t countIf(Functor f, DataTable& table)
{
    int32_t amount = 0;

    auto idIter = table.ids.begin();
    auto dataIter = table.data.begin();

    for(; idIter != table.ids.end(); ++idIter, ++dataIter)
    {
        if(f(*idIter, *dataIter))
        {
            ++amount;
        }
    }

    return amount;
}

}
