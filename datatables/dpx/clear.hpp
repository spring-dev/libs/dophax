#pragma once
#include "tables.hpp"

namespace dpx
{

template <typename DataType>
void clear(DataTable<DataType, true>& table)
{
    table.ids.clear();
    table.data.clear();
    table.idIndices.clear();
    table.meta.sorted = true;
    table.disabledIds.clear();
    table.disabledData.clear();
}

template <typename DataType>
void clear(DataTable<DataType, false>& table)
{
    for(TableId id : table.ids)
        table.meta.idPool.release(id);

    table.ids.clear();
    table.data.clear();
    table.idIndices.clear();
    table.meta.sorted = true;
    table.disabledIds.clear();
    table.disabledData.clear();
}

}
