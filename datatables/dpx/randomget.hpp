#pragma once
#include "tables.hpp"
#include <random>

namespace dpx
{

template <typename DataTable, typename RandomGen>
TableEntry<typename DataTable::Type> randomGet(RandomGen& generator, DataTable& table)
{
    TH_ASSERT(!table.ids.empty(), "Cannot random get from an empty table");

    std::uniform_int_distribution<size_t> indexRange(0, table.ids.size() - 1u);
    size_t randomIndex = indexRange(generator);

    return {table.ids[randomIndex], table.data[randomIndex]};
}

template <typename DataTable, typename RandomGen>
TableEntry<typename DataTable::Type> randomGet(RandomGen& generator, const DataTable& table)
{
    return randomGet(generator, const_cast<DataTable&>(table));
}

template <typename DataTable, typename Functor, typename RandomGen>
TableEntry<typename DataTable::Type> randomGetIf(Functor f, RandomGen& generator, DataTable& table)
{
    std::vector<size_t> candidates;
    candidates.reserve(table.ids.size());

    for(size_t i = 0; i < table.ids.size(); ++i)
    {
        if(f(table.ids[i], table.data[i]))
            candidates.push_back(i);
    }

    std::uniform_int_distribution<size_t> indexRange(0, candidates.size() - 1u);

    size_t randomIndex = indexRange(generator);

    return {table.ids[randomIndex], table.data[randomIndex]};
}

template <typename DataTable, typename Functor, typename RandomGen>
TableEntry<typename DataTable::Type> randomGetIf(Functor f, RandomGen& generator, const DataTable& table)
{
    return randomGetIf(f, generator, const_cast<DataTable&>(table));
}

}
