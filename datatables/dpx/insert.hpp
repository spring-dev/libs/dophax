#pragma once
#include "tables.hpp"
#include <type_traits>
#include <thero/assert.hpp>

namespace dpx
{

template <typename DataTable>
TableEntry<typename DataTable::Type> insert(TableId id, typename DataTable::Type data, DataTable& table)
{
    TH_ASSERT(table.idIndices.count(id) == 0, "Trying to insert id " << id << " in table " << table.meta.name);
    
    static_assert(std::is_same<typename DataTable::ExternalId, std::true_type>::value, "Cannot provide ID manually to a table which keeps track of own IDs.");

    table.ids.push_back(id);
    table.data.emplace_back(std::move(data));
    table.idIndices[id] = table.ids.size() - 1;

    typename DataTable::Type& entry = table.data.back();

    if(table.ids.size() > 1 && table.ids[table.ids.size() - 2] > table.ids.back())
        table.meta.sorted = false;

    return {id, entry};
}

template <typename DataTable>
std::enable_if_t<std::is_same_v<typename DataTable::ExternalId, std::true_type>, TableEntry<typename DataTable::Type>> insert(std::pair<TableId, typename DataTable::Type> entry, DataTable& table)
{
    static_assert(std::is_same_v<typename DataTable::ExternalId, std::true_type>, "Cannot provide ID manually to a table which keeps track of own IDs.");

    return insert(entry.first, entry.second, table);
}

template <typename DataTable>
TableEntry<typename DataTable::Type> insert(typename DataTable::Type data, DataTable& table)
{
    static_assert(std::is_same<typename DataTable::ExternalId, std::false_type>::value, "Must provide ID manually to a table which doesn't keep track of own IDs.");
    TableId id = table.meta.nextId++;

    table.ids.push_back(id);
    table.data.emplace_back(std::move(data));
    table.idIndices[id] = table.ids.size() - 1;

    typename DataTable::Type& entry = table.data.back();

    if(table.ids.size() > 1 && table.ids[table.ids.size() - 2] > table.ids.back())
        table.meta.sorted = false;

    return {id, entry};
}

}
