#pragma once
#include "tables.hpp"
#include "tableliststring.hpp"
#include <thero/assert.hpp>

namespace dpx
{

template <typename Functor, typename Table>
auto callFirstMatching(TableId id, Functor&& f, Table&& table) -> auto
{
    typename std::decay_t<Table>::Type* found = findId(id, table);

    if(found)
        return f(id, *found);
    else
    {
        return f(id, table.data.back());
    }
}

template <typename Functor, typename Table, typename...TailTables>
auto callFirstMatching(TableId id, Functor&& f, Table&& table, TailTables&&... tail) -> auto
{
    typename std::decay_t<Table>::Type* found = findId(id, table);

    if(found)
        return f(id, *found);
    else
        return callFirstMatching(id, f, tail...);
}

template <typename Table>
int32_t countMatching(TableId id, Table&& table)
{
    return static_cast<int32_t>(has(id, table));
}

template <typename Table, typename...TailTables>
int32_t countMatching(TableId id, Table&& table, TailTables&&... tail)
{
    return static_cast<int32_t>(has(id, table)) + countMatching(id, tail...);
}

template <typename Functor, typename...TableTypes>
auto matchId(TableId id, Functor&& f, TableTypes&&... tables) -> auto
{
    TH_ASSERT(countMatching(id, tables...) == 1, "entry ID " << id << " exists " << countMatching(id, tables...) << " times in the given table list. Must be 1. Tables: " << tableListString(tables...));
    return callFirstMatching(id, f, tables...);
}
}
