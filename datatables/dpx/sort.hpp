#pragma once
#include "tables.hpp"
#include "indexhash.hpp"
#include <algorithm>
#include <vector>
#include <numeric>
#include <tracy.hpp>

namespace dpx
{

template <typename DataTable>
void applySort(const DataTable& cTable, const std::vector<size_t>& permutations)
{
    ZoneScoped

    DataTable& table = const_cast<DataTable&>(cTable);

    std::vector<TableId>& sortedIds = table.cache.idSortCache;
    sortedIds = table.ids;
    std::vector<typename DataTable::Type>& sortedData = table.cache.dataSortCache;
    sortedData.resize(table.data.size());

    std::transform(permutations.begin(), permutations.end(), sortedIds.begin(), [&] (std::size_t i)
    {
        return table.ids[i];
    });
    std::transform(permutations.begin(), permutations.end(), sortedData.begin(), [&] (std::size_t i)
    {
        return std::move(table.data[i]);
    });

    table.ids = sortedIds;
    table.data = sortedData;

    rebuildIndexHashOrderingOnly(table);
}

template <typename DataTable>
const std::vector<size_t>& sort(const DataTable& cTable)
{
    ZoneScoped
    DataTable& table = const_cast<DataTable&>(cTable);

    if(!table.meta.sorted)
    {
        std::vector<size_t>& permutations = table.cache.permutationCache;
        permutations.resize(table.ids.size());

        std::iota(permutations.begin(), permutations.end(), 0);
        std::sort(permutations.begin(), permutations.end(), [&] (size_t i, size_t j)
        {
            return table.ids[i] < table.ids[j];
        });

        applySort(table, permutations);
        table.meta.sorted = true;
    }
    else
    {
        table.cache.permutationCache.resize(table.ids.size());
        std::iota(table.cache.permutationCache.begin(), table.cache.permutationCache.end(), 0);
    }

    return table.cache.permutationCache;
}

template <typename DataTable, typename Compare>
const std::vector<size_t>& sort(const DataTable& cTable, Compare comp)
{
    ZoneScopedN("sort comp")
    DataTable& table = const_cast<DataTable&>(cTable);

    std::vector<size_t>& permutations = table.cache.permutationCache;
    permutations.resize(table.ids.size());

    std::iota(permutations.begin(), permutations.end(), 0);
    std::sort(permutations.begin(), permutations.end(), comp);

    applySort(table, permutations);

    table.meta.sorted = false;

    return table.cache.permutationCache;
}

}
