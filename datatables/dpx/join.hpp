#pragma once

#include <array>
#include <algorithm>
#include <functional>

#include "tables.hpp"
#include "sort.hpp"
#include "functiontraits.hpp"
#include <tracy.hpp>

namespace dpx
{

namespace detail
{
    template<size_t index, typename IdArray, typename IndicesArray>
    bool advanceIndices(const IdArray& ids, IndicesArray& indices, bool& stop, TableId& lowestId)
    {
        ZoneScoped
        while(ids[indices[index]] < lowestId)
        {
            ++indices[index];

            if(indices[index] == ids.size())
            {
                stop = true;
                return false;
            }
        }

        if(ids[indices[index]] > lowestId)
        {
            lowestId = ids[indices[index]];
            return false;
        }

        return true;
    };

    template <typename Functor, typename...Args, std::size_t...staticIndexes>
    void join_impl(Functor&& functor, std::index_sequence<staticIndexes...>, Args&&...args)
    {
        if((args.ids.empty() || ...))
            return;

        (sort(args), ...);

        constexpr size_t dataCount = sizeof...(Args);
        std::array<TableId, dataCount> indices{};

        TableId lowestId = std::max({args.ids[0]...});

        while(true)
        {
            bool stop = false; 

            if((advanceIndices<staticIndexes>(args.ids, indices, stop, lowestId) && ...))
            {
                functor(lowestId, args.data[indices[staticIndexes]]...);

                lowestId++;

                if(((args.ids.size() == ++indices[staticIndexes]) || ...))
                    return;
            }
            else if(stop)
                return;

        }
    }
}

template <typename T>
struct type
{
};

template <typename DataStorage, typename Func, typename Return, typename IdType, typename... Args>
void join(DataStorage& storage, Func&& func, FunctionTraits<Return, IdType, Args...>)
{
    join(func, (storage.getTable(type<std::decay_t<Args>>{}))...);
}

template <typename DataStorage, typename Func>
void join(DataStorage& storage, Func&& func)
{
    join(storage, func, getFunctionTraits(func));
}

template <typename Functor, typename...Args>
void join(Functor&& functor, Args&&...args)
{
    detail::join_impl(functor, std::make_index_sequence<sizeof...(Args)>{}, args...);
}

}
