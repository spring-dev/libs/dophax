#pragma once
#include "tables.hpp"
#include "sort.hpp"
#include <thero/assert.hpp>

namespace dpx
{

template <typename DataTable>
typename DataTable::Type& get(TableId id, DataTable& table)
{
    ZoneScoped;
    auto foundIndex = table.idIndices.find(id);
    TH_ASSERT(foundIndex != table.idIndices.end(), "tried to get id " << id << " from table '" << table.meta.name << "' which didn't have it");

    return table.data[foundIndex->second];
}

template <typename DataTable>
const typename DataTable::Type& get(TableId id, const DataTable& table)
{
    auto& nonConst = get(id, const_cast<DataTable&>(table));
    return nonConst;
}

template <typename DataTable>
TableEntry<typename DataTable::Type> getAny(DataTable& table)
{
    TH_ASSERT(!table.ids.empty(), "trying to get any entry from an empty table");
    return TableEntry<typename DataTable::Type>{table.ids.front(), table.data.front()};
}

template <typename DataTable>
const TableEntry<typename DataTable::Type> getAny(const DataTable& table)
{
    TH_ASSERT(!table.ids.empty(), "trying to get any entry from an empty table");
    return TableEntry<typename DataTable::Type>{table.ids.front(), table.data.front()};
}

}
