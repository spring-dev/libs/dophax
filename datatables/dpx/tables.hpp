#pragma once
#include <vector>
#include <functional>
#include <ska/flat_hash_map.hpp>
#include <string>
#include "tableid.hpp"

namespace dpx
{

template<typename DataType>
struct TableEntry
{
    using Type = DataType;
    TableId id;
    std::reference_wrapper<DataType> data;
};

struct TableMeta
{
    std::string name;
    std::string description;
    TableId nextId = static_cast<TableId>(dpx::Null + 1);
    //sort
    mutable bool sorted = true;
};

template <typename DataType>
struct TableCache
{
    mutable std::vector<size_t> permutationCache;
    mutable std::vector<TableId> idSortCache;
    mutable std::vector<DataType> dataSortCache;
};

template<typename DataType, bool externalId, bool disablableIn = false>
struct DataTable
{
    DataTable(std::string name = "unnamed") { meta.name = name;}
    DataTable(std::string name, std::string description) { meta.name = std::move(name); meta.description = std::move(description); }
    using Type = DataType;
    using ExternalId = std::integral_constant<bool, externalId>;
    static constexpr bool disablable() {return disablableIn; }
    std::vector<TableId> ids;
    std::vector<DataType> data;
    ska::flat_hash_map<dpx::TableId, size_t> idIndices;
    TableMeta meta;
    TableCache<DataType> cache;
    std::vector<TableId> disabledIds;
    std::vector<DataType> disabledData;
};
}
