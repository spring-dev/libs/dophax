#pragma once
#include "tables.hpp"
#include "sort.hpp"
#include <optional>
#include <tracy.hpp>

namespace dpx
{

template <typename DataTable>
typename DataTable::Type* findId(TableId id, DataTable& table)
{
    ZoneScoped

    auto indexIter = table.idIndices.find(id);

    return indexIter != table.idIndices.end() ? &table.data[indexIter->second] : nullptr;
}

template <typename DataTable>
typename DataTable::Type* findId(TableId id, const DataTable& table)
{
    auto nonConst = findId(id, const_cast<DataTable&>(table));

    if(nonConst)
        return nonConst;
    else
        return nullptr;
}

template <typename DataTable, typename Functor>
std::optional<TableEntry<typename DataTable::Type>> findOne(Functor f, DataTable& table)
{
    ZoneScoped

    for(size_t i = 0; i < table.ids.size(); ++i)
    {
        if(f(table.ids[i], table.data[i]))
            return TableEntry<typename DataTable::Type>{table.ids[i], table.data[i]};
    }

    return {};
}

template <typename DataTable, typename Functor>
std::optional<TableEntry<typename DataTable::Type>> findOne(Functor f, const DataTable& table)
{
    auto nonConst = findOne(f, const_cast<DataTable&>(table));

    if(nonConst)
        return nonConst;
    else
        return {};
}

template <typename DataTable, typename Functor>
std::vector<TableEntry<typename DataTable::Type>> findIf(Functor f, DataTable& table)
{
    ZoneScoped
    std::vector<TableEntry<typename DataTable::Type>> result;

    for(size_t i = 0; i < table.ids.size(); ++i)
    {
        if(f(table.ids[i], table.data[i]))
            result.emplace_back(TableEntry<typename DataTable::Type>{table.ids[i], table.data[i]});
    }

    return result;
}

template <typename DataTable, typename Functor>
std::vector<TableEntry<typename DataTable::Type>> findIf(Functor f, const DataTable& table)
{
    auto nonConst = findIf(f, const_cast<DataTable&>(table));

    if(nonConst)
        return nonConst;
    else
        return {};
}

}
