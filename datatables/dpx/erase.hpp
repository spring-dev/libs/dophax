#pragma once
#include <algorithm>
#include <cstdint>
#include "tables.hpp"
#include "enable.hpp"

namespace dpx
{

template <typename DataTable>
bool erase(TableId id, DataTable& table, bool eraseDisabled = true)
{
    if constexpr(table.disablable())
    {
        if(eraseDisabled)
            enable(id, table);
    }

    auto indexToEraseIter = table.idIndices.find(id);

    if(indexToEraseIter != table.idIndices.end())
    {
        size_t indexToErase = indexToEraseIter->second;

        if(indexToErase == table.ids.size() - 1)
        {//erase last entry
            table.data.pop_back();
            table.ids.pop_back();
            table.idIndices.erase(id);
        }
        else
        {//erase non-last entry
            table.data[indexToErase] = std::move(table.data.back());
            table.data.pop_back();

            table.ids[indexToErase] = table.ids.back();
            table.ids.pop_back();

            table.idIndices.erase(id); //erase old entry, it has no index anymore
            table.idIndices[table.ids[indexToErase]] = indexToErase; //previous back item is now positioned where the old entry was

            table.meta.sorted = false;
        }

        return true;
    }
    return false;
}

template <typename Functor, typename DataTable>
int32_t eraseIf(Functor f, DataTable& table, bool eraseDisabled = true)
{
    auto idIter = table.ids.begin();
    auto dataIter = table.data.begin();

    size_t beforeSize = table.ids.size();
    int32_t erasedAmount = 0;

    for(; idIter != table.ids.end();)
    {
        if(f(*idIter, *dataIter))
        {
            dpx::TableId erasedId = *idIter;
            size_t erasedIndex = table.idIndices[erasedId];

            if(erasedIndex == table.ids.size() - 1)
            {//erase last entry
                table.data.pop_back();
                table.ids.pop_back();
                table.idIndices.erase(erasedId);
            }
            else
            {//erase non-last entry
                table.data[erasedIndex] = std::move(table.data.back());
                table.data.pop_back();

                table.ids[erasedIndex] = table.ids.back();
                table.ids.pop_back();

                table.idIndices.erase(erasedId); //erase old entry, it has no index anymore
                table.idIndices[table.ids[erasedIndex]] = erasedIndex; //previous back item is now positioned where the old entry was
            }

            idIter = table.ids.begin() + erasedIndex;
            dataIter = table.data.begin() + erasedIndex;

            ++erasedAmount;
        }
        else
        {
            ++idIter;
            ++dataIter;
        }
    }

    if(beforeSize != table.ids.size())
        table.meta.sorted = false;


    if constexpr(table.disablable())
    {
        if(eraseDisabled)
        {
            auto idIter = table.disabledIds.begin();
            auto dataIter = table.disabledData.begin();

            for(; idIter != table.ids.end();)
            {
                if(f(*idIter, *dataIter))
                {
                    idIter = table.disabledIds.erase(idIter);
                    dataIter = table.disabledData.erase(dataIter);

                    ++erasedAmount;
                }
                else
                {
                    ++idIter;
                    ++dataIter;
                }
            }
        }
    }


    return erasedAmount;
}
}
