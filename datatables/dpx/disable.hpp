#pragma once
#include <cstdint>
#include "tables.hpp"
#include "find.hpp"
#include "insert.hpp"
#include "erase.hpp"
#include "enable.hpp"

namespace dpx
{

template <typename DataTable>
bool setDisabled(TableId id, bool disable, DataTable& table)
{
    if(disable)
        return disable(id, table);
    else
        return enable(id, table);
}

template <typename DataTable>
bool disable(TableId id, DataTable& table)
{
    if constexpr(DataTable::disablable())
    {
        auto* data = findId(id, table);

        if(data)
        {
            table.disabledData.push_back(std::move(*data));
            table.disabledIds.push_back(id);

            erase(id, table, false);
            return true;
        }
        else
            return false;
    }
    else
        return true;
}
}
