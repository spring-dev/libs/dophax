#pragma once
#include <cstdint>
#include "tables.hpp"
#include "insert.hpp"

namespace dpx
{
template <typename DataTable>
bool enable(TableId id, DataTable& table)
{
    if constexpr(DataTable::disablable())
    {
        auto disabledEntryIter = std::find(table.disabledIds.begin(), table.disabledIds.end(), id);

        if(disabledEntryIter != table.disabledIds.end())
        {
            size_t disabledIndex = std::distance(table.disabledIds.begin(), disabledEntryIter);

            insert(id, std::move(table.disabledData[disabledIndex]), table);

            table.disabledIds.erase(disabledEntryIter);
            table.disabledData.erase(table.disabledData.begin() + disabledIndex);

            return true;
        }
        else
            return false;
    }
    else
        return true;
}
}
