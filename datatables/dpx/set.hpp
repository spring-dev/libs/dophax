#pragma once
#include "find.hpp"
#include "tables.hpp"
#include "insert.hpp"

namespace dpx
{

template <typename DataType>
DataType& set(TableId id, DataType value, DataTable<DataType, true>& table)
{
    auto entry = findId(id, table);

    if(entry)
    {
        *entry = std::move(value);
        return *entry;
    }
    else
    {
        return insert(id, std::move(value), table).data;
    }
}

template <typename DataType>
DataType& set(TableId id, DataType value, DataTable<DataType, false>& table)
{
    auto entry = findId(id, table);

    if(!entry)
    {
        TH_ASSERT(false, "Cannot set value of id '" << id << "' in a table that doesn't have that ID and provides own IDs");
    }
    *entry = std::move(value);
    return *entry;
}

}
