#pragma once
#include <cstdint>

namespace dpx
{
using TableId = uint64_t;
constexpr TableId Null = static_cast<TableId>(0);
}
