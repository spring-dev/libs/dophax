#pragma once
#include "tables.hpp"

template <typename DataTable>
void rebuildIndexHashOrderingOnly(DataTable& table)
{
    for(size_t i = 0; i < table.ids.size(); ++i)
        table.idIndices[table.ids[i]] = i;
}

template <typename DataTable>
void rebuildIndexHash(DataTable& table)
{
    table.idIndices.clear();

    for(size_t i = 0; i < table.ids.size(); ++i)
        table.idIndices[table.ids[i]] = i;
}
