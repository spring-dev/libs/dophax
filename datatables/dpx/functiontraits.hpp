#pragma once
#include <functional>

template <typename Return, typename... Args>
struct FunctionTraits {};

template <typename Return, typename... Args>
constexpr auto getFunctionTraits(Return (*)(Args...)) -> FunctionTraits<Return, Args...> {
  return {};
}

template <typename ClassType, typename Return, typename... Args>
constexpr auto getFunctionTraits(Return (ClassType::*)(Args...))
    -> FunctionTraits<Return, Args...> {
  return {};
}

template <typename ClassType, typename Return, typename... Args>
constexpr auto getFunctionTraits(Return (ClassType::*)(Args...) const)
    -> FunctionTraits<Return, Args...> {
  return {};
}

template <typename Return, typename... Args>
constexpr auto getFunctionTraits(const std::function<Return(Args...)>&)
    -> FunctionTraits<Return, Args...> {
  return {};
}

template <typename T>
constexpr auto getFunctionTraits(T) -> decltype(getFunctionTraits(&T::operator())) {
  return {};
}

