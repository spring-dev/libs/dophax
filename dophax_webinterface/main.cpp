#include <iostream>
#include <sstream>
#include <string>
#include "server.hpp"

HttpRequest process(HttpRequest& r)
{
    HttpRequest response;

    auto path = split(r.path, "/", true);

    if(path.empty())
        response.body = "This is a Message from a server!";
    else if(path[0] == "test")
    {
        response.body = (r.params["num"] != "") ? "This is a number : " + r.params["num"] + "!" : "There is no number!";
        response.body += "<br>" + r.path;
        response.body += "<br>" + r.uri;
        for(auto pair : r.params)
            response.body += "<br>" + pair.first + ": " + pair.second;
    }
    else if(path[0] == "test2")
    {
        response.body = "<html><head><title>img test</title></head><body><img src=\"file/test.png\"></img></body></html>";
    }
    else if(path[0] == "test3")
    {
        response.body = "<html><head><title>img test</title></head><body><form enctype=\"multipart/form-data\" method=\"post\" action=\"test\">  Number:<br>  <input type=\"number\" name=\"num\" value=\"43\">  <br>  Last name:<br>  <input type=\"text\" name=\"lastname\" value=\"Mouse\">  <br>file:<br>  <input type=\"file\" name=\"file\" value=\"\">  <br><br>  <input type=\"submit\" value=\"Submit\"></form></body></html>";
//        response.body = "<html><head><title>img test</title></head><body><form method=\"post\" action=\"test\">  Number:<br>  <input type=\"number\" name=\"num\" value=\"43\">  <br>  Last name:<br>  <input type=\"text\" name=\"lastname\" value=\"Mouse\">  <br>file:<br>  <input type=\"file\" name=\"file\" value=\"\">  <br><br>  <input type=\"submit\" value=\"Submit\"></form></body></html>";
    }
    else if(path[0] == "file")
    {
        std::ifstream file(path[1], std::ifstream::binary);
        if(!file)
            return response;

        file.seekg(0, std::ios::end);
        std::streampos length(file.tellg());

        if (length)
        {
            file.seekg(0, std::ios::beg);
            response.body.resize(length);
            file.read(&response.body.front(), length);
        }

//        response.headers["content-type"] = "application/octet-stream";
//        response.headers["content-type"] = "text/plain";
        response.headers["content-type"] = "image/*";
        response.status = S_200;
    }
    else
    {
        response.status = S_404;
        response.body = "<html><head><title>Wrong URL</title></head><body><h1>Wrong URL</h1>Path is : &gt;" + r.path + "&lt;</body></html>";
    }

    return response;
}

int main()
{
    sockInit();

    auto listener = makeListener(8080, false);
    if(SOCKET_IS_INVALID(listener))
        std::cout << "ERROR on listen" << std::endl;

    while(true)
    {
        auto client = makeSocket(listener);
        if (SOCKET_IS_INVALID(client))
            std::cout << "ERROR on accept" << std::endl;

        auto request = HttpParser().receiveRequest(client);
        auto processed = process(request);
        auto response = createReponse(processed);

//        std::cout << response << std::endl;

        send(client, response.c_str(), response.size(), 0);

        sockClose(client);
    }

    sockClose(listener);

    sockQuit();

    return 0;
}
