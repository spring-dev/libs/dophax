#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <map>
#include <chrono>
#include <algorithm>
#include <cctype>
#include <vector>
#include <functional>
#include <fstream>
#include <ctime>

#include "statusCodes.hpp"
#include "methods.hpp"

#include "utility.hpp"
#include "smartEnum.hpp"
#include "portableSocket.hpp"

struct HttpRequest
{
    Method method;
    std::string uri;

    std::string path;
    std::map<std::string, std::string> params;

    std::map<std::string, std::string> headers;

    int32_t httpVerMinor = 1;
    int32_t httpVerMajor = 1;

    Status status = S_200;

    std::string body;
};

struct HttpParser
{
    enum StateStatus
    {
        READING,
        CONTINUE,
        OK,
        FAIL,
    };

    using State = std::function<StateStatus(HttpParser*)>;
    using StateStack = std::vector<State>;

    StateStack states;
    StateStatus lastStatus;

    HttpRequest httpRequest;

    constexpr static auto SPACES = " \t";

    std::string request;
    size_t pos = 0;

    bool receiveDone = false;

    void pushState(const State& state)
    {
        states.push_back(state);
    }

    template <typename...Args>
    void pushState(const State& state, Args&&...args)
    {
        pushState(args...);
        pushState(state);
    }

    StateStatus parsePath()
    {
        pos = httpRequest.uri.find("?");
        httpRequest.path = httpRequest.uri.substr(0, pos);

        std::string params;
        if(pos != std::string::npos)
            params = httpRequest.uri.substr(pos + 1);

        auto it = httpRequest.headers.find("content-type");
        if(it != httpRequest.headers.end() && it->second == "application/x-www-form-urlencoded")
            params += "&" + httpRequest.body;

        if(!params.empty())
        {
            for(const auto& param : split(params, "&", true))
            {
                pos = param.find("=");

                if(pos == 0 || param.empty())
                    continue;

                std::string key = param.substr(0, pos);
                std::string value = "";
                if(pos != std::string::npos)
                    value = param.substr(pos + 1);

                httpRequest.params[key] = value;
            }
        }

        return OK;
    }

    StateStatus removeLeadingSpaces()
    {
        pos = request.find_last_of(SPACES, pos);

        if(pos != std::string::npos)
        {
            if(pos == request.size() - 1)
                return READING;

            request = request.substr(pos + 1);
            pos = 0;

            return OK;
        }
        else
        {
            pos = 0;
            return FAIL;
        }
    }

    StateStatus requireOneSpaceOrMore()
    {
        pushState(&HttpParser::removeLeadingSpaces, &HttpParser::assertStatusIsOk);
        return OK;
    }

    StateStatus waitEnfOfLine()
    {
        pos = request.find('\n', pos);
        if(pos == std::string::npos)
            return READING;
        else
            return OK;
    }

    StateStatus removeEndOfLine()
    {
        if(request[0] == '\n')
        {
            request = request.substr(1);
            return OK;
        }
        else if(request.size() == 1 && request[0] == '\r')
            return READING;
        else if(request.substr(0, 2) == "\r\n")
        {
            request = request.substr(2);
            return OK;
        }

        return FAIL;
    }

    StateStatus requireEndOfLine()
    {
        pushState(&HttpParser::removeEndOfLine, &HttpParser::assertStatusIsOk);
        return OK;
    }

    StateStatus assertStatusIsOk()
    {
        assert(lastStatus == OK);
        return OK;
    }

    StateStatus parseMethod()
    {
        pos = request.find_first_of(SPACES, pos);
        if(pos == std::string::npos)
            return READING;

        httpRequest.method = toMethod(request.substr(0, pos));
        request = request.substr(pos);
        pos = 0;

        pushState(&HttpParser::requireOneSpaceOrMore, &HttpParser::parseUri);
        return OK;
    }

    StateStatus parseUri()
    {
        pos = request.find_first_of(SPACES, pos);
        if(pos == std::string::npos)
            return READING;

        httpRequest.uri = decodeUri(request.substr(0, pos));
        request = request.substr(pos);

        pushState(&HttpParser::requireOneSpaceOrMore, &HttpParser::parseHttpVer);
        return OK;
    }

    StateStatus parseHttpVer()
    {
        if(request.size() < 8)
            return READING;

        assert(request.substr(0, 4) == "HTTP");
        request = request.substr(4);

        assert(request[0] == '/' && request[2] == '.' && "UPDATE THE VERSION CHECK YOU MORRON (atm it's checking for HTTP/x.x");

        httpRequest.httpVerMajor = request[1] - '0';
        httpRequest.httpVerMinor = request[3] - '0';
        request = request.substr(4);

        pushState(&HttpParser::removeLeadingSpaces, &HttpParser::requireEndOfLine, &HttpParser::startParsingHeader);
        return OK;
    }

    StateStatus startParsingHeader()
    {
        pushState(&HttpParser::waitEnfOfLine, &HttpParser::removeEndOfLine, &HttpParser::parseHeader);
        return OK;
    }

    StateStatus parseHeader()
    {
        if(request[0] == '\n' || request.substr(0, 2) == "\r\n")
        {
            pushState(&HttpParser::headerEnd);
            return OK;
        }

        pushState(&HttpParser::requireEndOfLine, &HttpParser::waitEnfOfLine);

        pos = request.find("\r\n");
        if(pos == std::string::npos)
            pos = request.find("\n");

        assert(pos != std::string::npos);
        std::string line = request.substr(0, pos);
        request = request.substr(pos);

        pos = line.find(":");
        assert(pos != std::string::npos && pos);

        std::string header = line.substr(0, pos);
        std::string value = line.substr(pos + 1);

        pos = header.find_first_of(SPACES);
        header = header.substr(0, pos);
        std::transform(header.begin(), header.end(), header.begin(), ::tolower);

        pos = value.find_first_not_of(SPACES);
        if(pos != std::string::npos)
            value = value.substr(pos);

        httpRequest.headers[header] = value;

        return CONTINUE;
    }

    StateStatus headerEnd()
    {
        //TODO refactor the whole function as there is much repetition and calculation of uneeded data

        bool hasBody = false;
        bool skipBody = false;

        bool isChunked = false;

        size_t contentLength = -1;
        bool hasContentLength = false;

        auto it = httpRequest.headers.find("content-length");
        if(it != httpRequest.headers.end())
            contentLength = std::stoi(it->second);
        hasContentLength = (contentLength > 0 && contentLength != (size_t)-1);

        it = httpRequest.headers.find("transfer-encoding");
        if(it != httpRequest.headers.end())
            isChunked = it->second == "chunked";

        skipBody = httpRequest.method == CONNECT || httpRequest.method == HEAD;
        hasBody = !skipBody && (isChunked || hasContentLength);

        //should not have content lenght and chunked encoding
        assert(!(isChunked && hasContentLength));

        std::cout << "contentLenght :" << contentLength << std::endl;
        std::cout << "hasContentLength :" << hasContentLength << std::endl;
        std::cout << "isChunked :" << isChunked << std::endl;

        if(hasBody)
        {
            if(hasContentLength)
                pushState(&HttpParser::requireEndOfLine, &HttpParser::readBody);
            else if(isChunked)
                pushState(&HttpParser::requireEndOfLine, &HttpParser::readChunked);
            else
                assert(false);
        }
        else
            receiveDone = true;

        std::cout << "header end" << std::endl;
        return OK;
    }

    StateStatus readBody()
    {
        std::cout << "readBody" << std::endl;

        //TODO cache content-length
        auto it = httpRequest.headers.find("content-length");
        assert(it != httpRequest.headers.end());

        size_t contentLength = std::stoi(it->second);
        std::cout << "contentLength: " << contentLength << std::endl;
        std::cout << "bodyLenght: " << request.size() << std::endl;
        std::cout << "bodyLenght: " << httpRequest.body.size() << std::endl;

        if(request.size() < contentLength)
            return READING;

        httpRequest.body = request.substr(0, contentLength);
        request = request.substr(contentLength);

        receiveDone = true;

        return OK;
    }

    StateStatus readChunked()
    {
        std::cout << "readChunked not implemented" << std::endl;
        receiveDone = true;
        return FAIL;
    }

    void begin(bool parsePath = true)
    {
        httpRequest = HttpRequest();
        receiveDone = false;
        lastStatus = OK;
        pos = 0;

        states.clear();
        request.clear();

        if(parsePath)
            pushState(&HttpParser::parsePath);

        pushState(&HttpParser::parseMethod);
    }

    StateStatus read(const char* data, size_t size)
    {
        pos = request.size();
        request.append(data, data + size);

        while(true)
        {
            if(states.empty())
                return lastStatus;

            auto it = states.size() - 1;
            lastStatus = states.back()(this);

            if(lastStatus != READING && lastStatus != CONTINUE)
            {
                states.erase(states.begin() + it);
                pos = 0;
            }

            if(lastStatus == READING || (!receiveDone && request.empty()))
                return READING;

            continue;
        }
    }

    HttpRequest receiveRequest(SOCKET s, bool parsePath = true)
    {
        begin(parsePath);

        while(true)
        {
            char buffer[512];
            auto size = recv(s, buffer, sizeof(buffer), 0);

            assert(size != SOCKET_ERROR);

            if(size == 0)
            {
                std::cout << "CONNECTION CLOSED" << std::endl;
                sockClose(s);
                break;
            }

            std::cout << std::string(buffer, size) << std::endl;

            auto status = read(buffer, size);
            if(status != READING)
                break;
        }


        std::cout << "Method: " << methodsString[httpRequest.method] << std::endl;
        std::cout << "Uri: " << httpRequest.uri << std::endl;
        std::cout << "Path: " << httpRequest.path << std::endl;

        for(auto& pair : httpRequest.params)
            std::cout << pair.first << ": " << pair.second << std::endl;

        std::cout << "Http Ver: HTTP/" << +httpRequest.httpVerMajor << "." << +httpRequest.httpVerMinor << std::endl;

        for(auto& pair : httpRequest.headers)
            std::cout << pair.first << ": " << pair.second << std::endl;

        std::cout << std::endl;
        std::cout << std::endl;
        std::cout << "BODY" << std::endl;
        std::cout << "***" << std::endl;
        std::cout << httpRequest.body << std::endl;
        std::cout << "***" << std::endl;

        std::cout << std::endl;
        std::cout << std::endl;

        return httpRequest;
    }
};

std::string createReponse(HttpRequest& r)
{
    static const std::string LINE_END = "\r\n";

    std::string response;

    response += std::string("HTTP/") + char(r.httpVerMajor + '0') + "." + char(r.httpVerMinor + '0') + " ";
    response += statusString[r.status] + LINE_END;

    time_t ltime;
    time(&ltime);
    tm* gmt= gmtime(&ltime);
    char* asctime_remove_nl = asctime(gmt);
    asctime_remove_nl[24] = 0;

    r.headers["date"] = asctime_remove_nl;
    r.headers["server"] = "HaxServor";
    r.headers["connection"] = "close";
    if(r.headers["content-type"] == "")
        r.headers["content-type"] = "text/html; charset=ISO-8859-1";
    r.headers["content-lenght"] = std::to_string(r.body.size());

//    CONTENT_TYPE_MAPPING = {
//  'html' => 'text/html',
//  'txt' => 'text/plain',
//  'png' => 'image/png',
//  'jpg' => 'image/jpeg'
//}
//
//# Treat as binary data if content type cannot be found
//DEFAULT_CONTENT_TYPE = 'application/octet-stream'

    for(const auto& pair : r.headers)
        response += pair.first + ": " + pair.second + LINE_END;

    std::cout << response << std::endl;
    std::cout << r.body.size() << std::endl;

    response += LINE_END;
    response += r.body;

    return response;
}

uint64_t getTimeMs()
{
    using namespace std::chrono;
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

SOCKET makeListener(int port, bool blocking = true)
{
    auto listSocket = socket(AF_INET, SOCK_STREAM, 0);
    if(SOCKET_IS_INVALID(listSocket))
        return listSocket;

    int iSetOption = 1;
    setsockopt(listSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&iSetOption,sizeof(iSetOption));

    struct sockaddr_in listAddr;
    memset(&listAddr, 0, sizeof(listAddr));

    listAddr.sin_family = AF_INET;
    listAddr.sin_addr.s_addr = INADDR_ANY;
    listAddr.sin_port = htons(port);

    auto error = bind(listSocket, (struct sockaddr *) &listAddr, sizeof(listAddr));
    if(error == SOCKET_ERROR)
        return error;

    listen(listSocket, 5);

    setBlocking(listSocket, blocking);

    return listSocket;
}

SOCKET makeSocket(SOCKET listener, uint64_t timeout = 0, bool blocking = true)
{
    auto newSocket = INVALID_SOCKET;

    struct sockaddr_in newSocketAddr;
    SOCKLEN addrSize = sizeof(newSocketAddr);

    auto time = getTimeMs();

    while(SOCKET_IS_INVALID(newSocket) && (timeout == 0 || getTimeMs() - time < timeout))
        newSocket = accept(listener, (struct sockaddr*)&newSocketAddr, &addrSize);

    setBlocking(newSocket, blocking);

    return newSocket;

}
