#pragma once

enum Method {GET, HEAD, POST, OPTIONS, CONNECT, TRACE, PUT, PATCH, DELETE};

static Method toMethod(std::string method)
{
    if(method == "GET") return GET;
    else if(method == "HEAD") return HEAD;
    else if(method == "POST") return POST;
    else if(method == "OPTIONS") return OPTIONS;
    else if(method == "CONNECT") return CONNECT;
    else if(method == "TRACE") return TRACE;
    else if(method == "PUT") return PUT;
    else if(method == "PATCH") return PATCH;
    else if(method == "DELETE") return DELETE;

    assert(false && "INVALID METHOD");
}

static const char* methodsString[] =
{
    "GET",
    "HEAD",
    "POST",
    "OPTIONS",
    "CONNECT",
    "TRACE",
    "PUT",
    "PATCH",
    "DELETE"
};
