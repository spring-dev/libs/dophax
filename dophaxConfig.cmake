include(${CMAKE_CURRENT_LIST_DIR}/dophax-config.cmake)

set(DOPHAX_BIN ${CMAKE_CURRENT_LIST_DIR}/../../../bin/dophax_gen)

function(add_dophax_target NAME DPX)
    set(OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/${NAME})

    execute_process(
            COMMAND ${DOPHAX_BIN} ${CMAKE_CURRENT_SOURCE_DIR}/${DPX} --dependencies-report
            OUTPUT_VARIABLE DEPENDENCIES
    )
    string(REPLACE " " ";" DEPENDENCIES ${DEPENDENCIES})

    execute_process(
            COMMAND ${DOPHAX_BIN} ${CMAKE_CURRENT_SOURCE_DIR}/${DPX} ${OUTPUT_DIR} --dependents-report
            OUTPUT_VARIABLE OUTPUT
    )
    string(REPLACE " " ";" OUTPUT ${OUTPUT})

    message(${OUTPUT})

    add_custom_command(
            COMMAND ${DOPHAX_BIN} ${CMAKE_CURRENT_SOURCE_DIR}/${DPX} ${OUTPUT_DIR}
            DEPENDS ${DEPENDENCIES}
            OUTPUT ${OUTPUT}
    )

    add_library(${NAME} ${OUTPUT})
    target_include_directories(${NAME} PUBLIC $<BUILD_INTERFACE:${OUTPUT_DIR}/generated>)
    target_link_libraries(${NAME} PRIVATE dophax::dophax)
endfunction()
